/**
 * Created by Sarath Kumar on 07-01-2016.
 */
var _ = require("underscore");
var moment = require("moment");
var crypto = require("crypto");
module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db, app);
    var ApiMiddleware = require("./../apiMiddleware.js")(db, app);

    app.get("/api/getKey", middleware.RequireAuth, function (req, res) {
        var ID = req.current_user.id;
        db.API.findOne(
            {
                where: {
                    USerId: ID
                }
            }
        ).then(function (api) {
                console.log(api)
                if (api == null) {
                    res.json({
                        status: false
                    })

                } else {
                    res.json({
                        status: true,
                        api: api
                    })
                }

            }, function () {

            })


    });
    app.post("/api/createkey", middleware.RequireAuth, function (req, res) {
        var ID = req.current_user.id;
        db.API.findOne(
            {
                where: {
                    USerId: ID
                }
            }
        ).then(function (api) {
                console.log(api);
                if (api == null) {
                    var current_date = (new Date()).valueOf().toString();
                    var random = Math.random().toString();
                    var string = crypto.createHash('sha1').update(current_date + random).digest('hex');
                    var apiObj = {
                        Key: string,
                        UserId: ID
                    };

                    db.API.create(apiObj).then(function (Apiobj) {
                        if (apiObj != null) {
                            console.log(Apiobj);
                            res.json(Apiobj);
                        } else {
                            res.sendStatus(500);

                        }

                    }, function (eroor) {
                        res.sendStatus(500);

                    });


                } else {
                    res.json(api);

                }

            }, function () {

            })


    });
    app.post("/api/deletkey", middleware.RequireAuth, function (req, res) {
        var ID = req.current_user.id;
        db.API.findOne(
            {
                where: {
                    USerId: ID
                }
            }
        ).then(function (api) {
                console.log(api);
                if (api != null) {
                    api.destroy().then(function () {
                        res.json({
                            status: "Success"
                        });
                    })

                }

            }, function () {

            })


    });

    app.post("/service/api/GenerateToken", ApiMiddleware.ApiAuth, function (req, res) {
        /*Required
         * callback
         * amount
         * type
         *
         * expire
         *
         * */
        var data = req.body;
        console.log(data)
        
        if (!_.has(data, "Callback")) {

            res.status(400).json({Error: "bad Parameters"});
        } else if (!_.has(data, "amount")) {

            res.status(400).json({Error: "bad Parameters"});
        } else if (!_.has(data, "type")) {

            res.status(400).json({Error: "bad Parameters"});
        } else {
            var type = "";
            var expire = 60;
            if (data.type == "Dynamic") {
                type = "Dynamic";
                expire = data.expire;

            } else if (data.type == "static") {
                type = "Static";
                expire = data.expire;

            } else if (data.type == "donation") {
                type = "Donate";
            } else {
                res.status(400).json({Error: "bad Parameters"});
                return;
            }
            console.log(data);
            var randomString = crypto.randomBytes(32).toString('hex');
            var token = {
                amount: data.amount,
                type: "BOTH",
                UseCounter: 0,
                ApiId: req.api.id,
                Tokentype: type,
                callback: data.Callback,
                Expire: moment().add(expire, "m").format('x')
            };

            token.TokenHASH = randomString;
            console.log(token);

            db.paymentTokens.create(token).then(function (instance) {
                db.Users.findById(req.current_user).then(function (user) {
                    instance.update({UserId: user.get('id')}).then(function () {
                        var response = _.pick(instance, "amount", "id", "TokenHASH", "callback", "type", "createdAt");
                        res.json(response);


                    }, function (error) {
                        res.status(401).json({Error: "Something Went Wrong"})
                        console.log(error)


                    });
                });
            }, function (error) {
                console.log(error)

                res.status(401).json({Error: "Something Went Wrong"})
            });

        }


    });
    app.post("/service/api/status", ApiMiddleware.ApiAuth, function (req, res) {

        var data = req.body;
        if (!_.has(data, "id")) {
            res.status(400).json({Error: "bad Parameters"});

        } else {
            var id = data.id;

            db.paymentTokens.findOne({
                where:{
                    id:id,
                    UserId:req.api.UserId
                }
            }).then(function (instance) {
                if (instance == null) {
                    res.status(400).json({Error: "Wrong ID"});
                } else {
                    res.json(_.pick(instance,"amount","ispaid","TokenHASH","UseCounter","callback"));

                }
            },function(){
                res.status(401).json({Error:"Id not found"})

            });
        }


    });
    app.get("/service/api/getAlltokens", ApiMiddleware.ApiAuth, function (req, res) {
            db.paymentTokens.findAll({
                where:{
                    UserId:req.api.UserId,
                    ApiId:req.api.id
                },
                attributes: ['id', 'amount', 'UseCounter', 'ispaid', 'callback', 'type','Expire']

            }).then(function (instance) {
                if (instance == null)
                {
                    res.status(400).json({Error: "Wrong ID"});
                } else {
                    res.json(instance);

                }
            },function(){
                res.status(401).json({Error:"Id not found"})

            });


    })


};
