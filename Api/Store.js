/**
 * Created by Sarath Kumar on 10-01-2016.
 */
var _ = require("underscore");
var moment = require("moment");
var crypto = require("crypto");
module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db, app);
    app.post("/api/store/create", middleware.RequireMerchant, function (req, res) {
        var body = _.pick(req.body, 'StoreName', 'Desc', 'OpenTime', 'CloseTime', 'StoreTypes', 'Address');
        body.UserId = req.current_user.id;
        db.Store.create(body).then(function (Store) {
            res.json(Store);
        }, function (error) {
            res.status(400).json(error);
        });
    });
    app.delete("/api/store/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: ID
            }
        }).then(function (Store) {
            if (Store != null) {
                Store.destroy().then(function (rowsDeleted) {
                    if (rowsDeleted === 0) {
                        res.sendStatus(500).json({error: "NO STORE WITH THE GIVEN ID"})
                    } else {
                        res.sendStatus(204);

                    }

                }, function (error) {
                    res.sendStatus(500).json(error)
                })
            } else {
                res.sendStatus(404);

            }

        }, function () {
            res.sendStatus(404).json({error: "NOT FIOUND"})

        });


    });
    app.put("/api/store/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        var body = _.pick(req.body, 'StoreName', 'Desc', 'OpenTime', 'CloseTime', 'StoreTypes', 'Address');

        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: ID
            }
        }).then(function (Store) {

            Store.update(body).then(function (UpdatedStore) {
                res.json(UpdatedStore);
            }, function (error) {
                res.sendStatus(500).json(error)
            })
        }, function () {
            res.sendStatus(404).json({error: "NOT FIOUND"})

        });


    });
    app.get("/api/stores/", middleware.RequireMerchant, function (req, res) {
        db.Store.findAll({
            where: {
                UserId: req.current_user.id
            }
        }).then(function (Stores) {
            res.json(Stores)
        }, function (error) {
            res.json(error)
        });

    });
    app.get("/api/Store/:id", middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Store.findById(ID).then(function (store) {
            res.json(store);
        }, function (error) {
            res.sendStatus(404);
        });
    });
    app.post("/api/Store/pic/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        try {
            if (req.body.bs64 != undefined) {
                db.Store.find({
                    where: {
                        UserId: req.current_user.id,
                        id: ID
                    }
                }).then(function (Store) {
                    if (Store != null) {
                        var base64Data = req.body.bs64.replace(/^data:image\/png;base64,/, "");
                        var filename = require("crypto-js").MD5("profile_" + Date.now()).toString() + ".png";
                        require("fs").writeFile(process.cwd() + "/public/store/" + filename, base64Data, 'base64', function (err) {
                            if (err) {
                                res.status(500).send();
                            } else {
                                var attributes = {StorePic: filename};
                                Store.update(attributes).then(function (store) {
                                    res.json(store);
                                }, function () {
                                    res.sendStatus(500);
                                });

                            }
                        });
                    }
                }, function () {
                    res.sendStatus(401);

                });
            }


        } catch (err) {
            res.sendStatus(500);
        }
    })

    app.get("/api/Store/token/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: ID
            }
        }).then(function (Store) {
             if(Store) {
                 db.paymentTokens.findOne(
                     {
                         where: {StoreId: Store.id}
                     }).then(function (Token) {
                          if (Token) {
                             res.json(Token);
                         } else {
                             //not found need to create one
                             var DynamicToken = {
                                 amount: 0,
                                 type: "BOTH",
                                 UseCounter: 0,
                                 Tokentype: "Dynamic",
                                 StoreId: ID,
                                 Expire: 0
                             };
                             var randomString = crypto.randomBytes(32).toString('hex');
                             DynamicToken.TokenHASH = randomString;
                             db.paymentTokens.create(DynamicToken).then(function (dynamicPayIns) {
                                 res.json(dynamicPayIns);
                             }, function (error) {

                                 res.sendStatus(401);
                             });
                         }


                     }, function (error) {
                         res.sendStatus(404);

                     });
             }else{
                 res.sendStatus(404);

             }

        },function(){
            res.sendStatus(404);

        });


    });

};
