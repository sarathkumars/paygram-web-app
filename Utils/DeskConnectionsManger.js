/**
 * Created by Sarath Kumar on 07-02-2016.
 */
var _ = require("underscore");
var loki = require('lokijs');
LokiDB = new loki('Desk_Online.json');
var CONNECTIONS = LokiDB.addCollection("DESK_CONNECTIONS")
var CLIENTS = LokiDB.addCollection("MOB_CLIENTS");
module.exports = {
    LOKI_CONNECTIONS: CONNECTIONS,
    LOKI_CLIENTS: CLIENTS,
    /**
     * @return {boolean}
     */
    AddRoom: function (RoomData) {
        var Room = CONNECTIONS.where(function (obj) {
            return obj.Storeid == RoomData.Storeid
        });
        if (Room != undefined && Room.length != 0) {
            console.log("UPDATE ROOM");
            Room.host = RoomData.host;
            var Room = CONNECTIONS.removeWhere(function (obj) {
                return obj.Storeid == RoomData.Storeid
            });
            CONNECTIONS.insert({
                host: RoomData.host,
                Storeid: RoomData.Storeid,
                productsCache: {}

            });
            return true;

        } else {
            console.log("INSERT ROOM");
            CONNECTIONS.insert({
                host: RoomData.host,
                Storeid: RoomData.Storeid,
                productsCache: {}
            });
            console.log(CONNECTIONS) ;

            CLIENTS.removeWhere(function (obj) {
                return obj.Storeid = RoomData.Storeid;
            });
            return true;
        }
    },
    /**
     * @return {boolean}
     */
    AddClient: function (StoreID, UserInfo) {
        var Room = CONNECTIONS.where(function (obj) {
            return obj.Storeid == StoreID;
        });
        if (Room != undefined && Room.length != 0) {
            var client = CLIENTS.where(function (obj) {
                console.log("FOUND OBJECT", obj);
                return obj.token == UserInfo.token;
            });
            console.log(client)

            if (client != undefined && client.length != 0) {
                return "RESERVED";
            } else {
                console.log(Room);
                CLIENTS.insert({
                    Storeid: Room[0].Storeid,
                    UserInfo: UserInfo,
                    socket:  UserInfo.socketId,
                    token: UserInfo.token,
                });
                console.log("TABLE CLIENT ADDED");

                return true;


            }
        }
        else {
            console.log("OFFLINE");

            return "OFFLINE"
        }


    },
    GetClient: function (id) {

        return   CLIENTS.findOne({token: id});

    },
    RemoveClient: function (token) {
        console.log(token);
        var data = CLIENTS.findOne({token: token});

        CLIENTS.remove(data);


    },
    /**
     * @return {boolean}
     */
    CheckOnline: function (id) {
        var Room = CONNECTIONS.where(function (obj) {
            return obj.Storeid == id;
        });
        return !!(Room != undefined && Room.length != 0);
    },

    GetHost: function (StoreID) {
        var host = CONNECTIONS.findOne({Storeid: StoreID}).host;

        return host;
    },


    getAllClients: function (StoreID) {
        var clients = CLIENTS.find({Storeid: StoreID});
        return clients;

    },
    ClientTokenCheck: function (token) {
        return CONNECTIONS.where(function (obj) {
            return obj.Storeid == StoreID
        })[0].host;
    }


};