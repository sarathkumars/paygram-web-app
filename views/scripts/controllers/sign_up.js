
/**
 * Created by Sarath Kumar on 23-12-2015.
 */

app.controller('signUp', function($scope,$http) {
    $scope.EMailexist = false;
    $scope.accountsuccess = false;
    $scope.accError = false;
    $scope.merchantAccount=false;
    $scope.merchant = function(){
        if($scope.merchantAccount==false){
            $scope.merchantAccount=true;
        }else{
            $scope.merchantAccount=false;
        }
    };
     $scope.signUP = function(){
         if($scope.user){
           if($scope.user.hasOwnProperty("Name") && $scope.user.hasOwnProperty("Email") && $scope.user.hasOwnProperty("password")
               && $scope.user.hasOwnProperty("Country") && $scope.user.hasOwnProperty("State")&& $scope.user.hasOwnProperty("zip")&& $scope.user.hasOwnProperty("address") ){
                 var URL =  "/api/customer/";
                console.log($scope.user);
               if($scope.merchantAccount ==true){
                      URL ="/api/merchant/";
               }
               $http.post(URL, JSON.stringify($scope.user))
                   .then(function(res)
                   {
                       $scope.accountsuccess = true;
                       $scope.accError = false;

                   },
                   function(response){
                       $scope.accError = true;
                       console.log(response.data)
                       $scope.accountsuccess = false;

                   } )
           }

         }
     };
    $scope.$watch("user.Email",function(email)
    {
        if(email)
        $http.get('/api/emailcheck?mail='+email).then(function(){
            $scope.EMailexist = true;
        }, function(data){
            $scope.EMailexist = false;
        });
    })
 });
