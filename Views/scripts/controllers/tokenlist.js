/**
 * Created by Sarath Kumar on 07-01-2016.
 */


app.controller('tokenctrl', ['$scope','$http','$mdToast', function ($scope,$http,$mdToast) {
    $scope.paytype = [
        {name: 'Paid', filter: 'true'},
        {name: 'Unpaid', filter: 'false'}

    ];
    $scope.tokenscheme = [
        {name: 'Static', filter: 'Static'},
        {name: 'Dynamic', filter: 'Dynamic'},
        {name: 'Donation', filter: 'Donation'}
    ];
    $scope.tokentype = [
        {name: 'NFC', filter: 'NFC'},
        {name: 'QR', filter: 'QR'},
        {name: 'BOTH', filter: 'BOTH'}
    ];

    $scope.tokendata = null;

    $scope.gettokens = function(){
        $http.get('/api/paytokens').then(function (resp) {
            $scope.tokendata = resp.data;
        });

    };
    $scope.gettokens();
    $scope.Delete = function(id){
        console.log(JSON.stringify({id:id}));

        $http.post("/api/token/delete", JSON.stringify({id:id})).then(function(){
            $mdToast.show(
                $mdToast.simple()
                    .content('Token Successfully Deleted')
                    .hideDelay(2500));
            $scope.gettokens();
        },function(error){
               console.log(error)

        })
    }

}]);
