/**
 * Created by Sarath Kumar on 13-01-2016.
 */
app.controller("asideCtrl", function (Auth, $state, $scope, $http, Socket, Evts,$rootScope) {

    if (Notification.permission !== 'denied')
    {
        Notification.requestPermission(function (permission) {

        });
    }
    $scope.OnlineStatsMob = "danger";
    $scope.logOut = function () {
        $http({
            method: 'POST',
            url: '/api/merchant/logOut'
        }).then(function () {
            $state.go('access.signin', {}, {reload: true});
            console.log($state)
        }, function () {
            $state.go('access.signin', {}, {reload: true});
        });
    };
    if (Socket.CONNECTED) {
        $scope.OnlineStats = "success";
    } else {
        $scope.OnlineStats = "danger";

    }
    $scope.$on(Evts.EVT_CONNECT, function () {
        $scope.OnlineStats = "success";
        $scope.$apply();
    });
    $scope.$on(Evts.EVT_DISCONNECT, function () {
        $scope.OnlineStats = "danger";
        $scope.$apply();
    });

    $scope.$on(Evts.EVT_RECONNECTING, function () {
        $scope.OnlineStats = "warning";
        $scope.$apply();

    });
    $scope.$on(Evts.EVT_RECONNECT_ER, function () {
        $scope.OnlineStats = "danger";
        console.log("CALLED ERROR RE");
        Socket.CONNECTED = false;
        $scope.$apply();

    });
    $scope.$on(Evts.EVT_NOITFY, function (evnts, data) {

        jQuery("#notifyIcon").addClass("animated infinite swing");
               var options = {
            body: "A Successful  Payment from : " + data.payee,
            icon: "/profile/" + data.photo,

        };
        if (Notification.permission === "granted") {
            var n = new Notification("Success",options);
            document.getElementById('notification').play();
            // If it's okay let's create a notification
            Auth.getUser();

        } else {
            Notification.requestPermission(function (permission) {
                // If the user accepts, let's create a notification
                if (permission === "granted")
                {
                    var n = new Notification("Success",options);
                    document.getElementById('notification').play();
                    Auth.getUser();
                }
            });
        }


    });
    $rootScope.changeNotfy = function(){
        jQuery("#notifyIcon").removeClass("animated infinite swing");

    };

    $scope.$on(Evts.MOB_STATE, function (events,onlineState)
    {
        console.log(onlineState)

               if (onlineState) {
            $scope.OnlineStatsMob = "success";
             $scope.$apply();
        }else{
             $scope.OnlineStatsMob = "danger";
             $scope.$apply();

         }

    });


});
