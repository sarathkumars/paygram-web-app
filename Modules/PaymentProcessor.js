/**
 * Created by Sarath Kumar on 13-12-2015.
 */

var request = require("request");

module.exports = function (db, payer, payee, token, TokenType,method) {
 return new Promise(function(resolve,reject){
     var trans;
      db.sequelize.transaction().then(function (t) {
          trans=t;
          var deduct = {
              Balance: parseInt(payer.Balance - token.amount)
          };
          var add = {
              Balance: parseInt(payee.Balance + token.amount)
          };
          return payer.update(deduct, {transaction: t}).then(function (payerIns)
          {
              return  payee.update(add, {transaction: t}).then(function (payeeIns)
              {
                  var Payment = {
                      amount: token.amount,
                      paymentType: token.type,
                      TokenType: TokenType,
                      payer: payerIns.id,
                      payee: payeeIns.id,
                      PaymentTokenId: token.id
                  };

                  return  db.payments.create(Payment,{transaction: t}).then(function (payment) {
                      if(TokenType==="Static")
                      {
                         return token.update({ispaid:true,UseCounter:1},{transaction: t}).then(function()
                         {

                             if(token.ApiId!=-1){

                                 APiCallback(token,payment,payer).then(function(data){
                                     if(data==true){
                                         t.commit();

                                     }
                                 },function(){
                                     throw new Error();
                                 });

                             }
                              else{

                                 t.commit();

                             }
                              return payment;
                          },function(error){
                             console.log("EROOR IN UPDATE");
                             throw new Error(error);

                         });


                      }else{
                          if(token.ApiId!=-1){

                              APiCallback(token,payment,payer).then(function(data){
                                  if(data==true){
                                      t.commit();

                                  }
                              },function(){
                                  throw new Error();
                              });

                          }
                          else{

                              t.commit();

                          }
                      }
                      return payment;
                  }, function (error) {
                      console.log(error);
                      throw new Error(error);  //rollback
                  });
              }, function (error)
              {
                  throw new Error(error);  //rollback

              });

          }, function (e) {
              console.log(e);
              throw new Error(e);  //rollback

          });

      }).then(function (result) {
           resolve(result);

      }).catch(function (err) {
          console.log("TOKEN UPDATE :error",err);
          trans.rollback();
          reject(err);

      });


  });


    function APiCallback(token,payment,payer){
        return new Promise(function(resolve,reject) {

            if (token.callback != null) {
                var body = {
                    tokenID: token.id,
                    transcationID: payment.id,
                    user: payer.Name,
                    amount: token.amount,
                    status: 'sucess'
                };

                var options = {
                    method: 'POST',
                    url: token.callback,
                    headers: {
                        'cache-control': 'no-cache',
                        'content-type': 'application/json',
                    },
                    body: body,
                    json: true
                };

                request(options, function (error, response, body) {

                    console.log(typeof response.statusCode)
                    if (response.statusCode == 200) {


                         resolve(true);

                    } else {

                        reject(false);

                    }

                });

            } else {
                return false;
            }

        });
    }
};