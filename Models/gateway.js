/**
 * Created by Sarath Kumar on 10-04-2016.
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('gatewayResponse', {
            pay_id: {
                type: DataTypes.STRING,
                allowNull: false
            },
            create_time: {
                type: DataTypes.STRING,
                allowNull: false
            },
            state: {
                type: DataTypes.STRING,
                allowNull: false
            },
            amount: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            currency: {
                type: DataTypes.STRING,
                allowNull: false
            },
            card_number: {
                type: DataTypes.INTEGER,
                allowNull: false
            },

            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            card_type: {
                type: DataTypes.STRING,
                allowNull: true
            }

        }
    )
        ;

}

;