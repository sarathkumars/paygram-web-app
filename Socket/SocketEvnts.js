/**
 * Created by Sarath Kumar on 13-01-2016.
 */
module.exports = {
    EVT_CONNECT: "connect",
    EVT_CHECKS: "checks",
    EVT_CHECKS_REPLY: "checksrpl",
    EVT_LOGOUT: "lgt",
    EVT_DISCONNECT: "disconnect",
    EVT_LEAVE: "lev",
    RE_INIT_STORE: "reinitst",
    EVT_PAYMENT: "reinitst",
    EVT_NOITFY: "Notify",
    EVT_JOIN: "join",
    EVT_STATES: "state",
    EVT_JOIN_REPLY: "joinrpl",
    EVT_WRITE_NFC: "Nfcwr",
    EVT_CODE_VALIDATE: "codeval",
    EVT_VALIDATE_REPLY: "codereplay",
    EVT_PROCESS_PAYMENT: "payprocess",
    EVT_PROCESS_PAYMENT_REPLY: "payprocessrpl",
    MOB_STATE: "mobStat",
    LAUNCH_DESK: "launDes",
    EVT_ORDER: "order",
    EVT_ORDER_REPLY: "order_rpl"

};

