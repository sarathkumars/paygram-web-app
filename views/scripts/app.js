'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
var app = angular.module('app', [
    'ngAnimate',
    'colorpicker.module',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngMaterial',
    'ngStorage',
    'ui.router',
    'ui.utils',
    'ui.bootstrap',
    'ui.load',
    'ui.jp',
    'oc.lazyLoad',
    'angular-loading-bar'
]);
app.factory("Evts", function () {
    return {
        EVT_CONNECT: "connect",
        EVT_LOGOUT: "lgt",
        EVT_DISCONNECT: "disconnect",
        EVT_NOITFY: "Notify",
        EVT_STATES: "state",
        EVT_RECONNECTING: "reconnecting",
        EVT_RECONNECT: "reconnect",
        EVT_ORDER_RPLY: "order_rpl",
        EVT_RECONNECT_ER: "reconnect_error",
        EVT_WRITE_NFC :"Nfcwr",
        MOB_STATE :"mobStat",
        EVT_LEAVE: "lev",
        EVT_JOIN: "join",
        EVT_JOIN_REPLY: "joinrpl",
        RE_INIT_STORE: "reinitst",
        LAUNCH_DESK: "launDes",
        EVT_ORDER: "order"


    }
});


app.factory("Auth", function ($http, $q, $rootScope) {
    var AuthObj = {};
    AuthObj.user = null;
    AuthObj.Ismerchant = false;
    AuthObj.isAuthenticated = function () {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/api/isAuthenticated'
        })
            .then(function (data) {
                if (data.status == 200) {
                    defer.resolve(true);
                    AuthObj.Ismerchant = data.data.Ismerchant
                } else {
                    defer.reject(false);
                }
            }, function (data) {
                console.log(data);
                defer.reject(false);
            });
        return defer.promise;
    };
    AuthObj.getUser = function () {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/api/merchant'
        })
            .then(function (res) {
                AuthObj.user = res.data;
                $rootScope.LogUser = res.data;
                $rootScope.MerchantUser = true;
                defer.resolve(res.data);
            }, function (data) {
                defer.reject(data);
            });
        return defer.promise

    };
    return AuthObj;
})
    .run(
    ['$rootScope', '$state', '$stateParams', 'Auth',
        function ($rootScope, $state, $stateParams, Auth) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toState.requireAuth) {
                    if (toState.requireAuth == true) {
                        Auth.isAuthenticated().then(function () {
                            Auth.getUser().then(function () {
                                if (Auth.user.profilepic == null) {
                                    Auth.user.profilepic = "default.png";
                                }
                                $state.go(toState.name);
                            });
                        }, function () {

                            event.preventDefault();
                            $state.go("access.signin");

                        });
                    } else {
                        if (toState.name == "access.signin") {
                            event.preventDefault();
                            $state.go("app.dashboard");
                        }
                    }
                } else {

                }

            });
        }
    ]
);




