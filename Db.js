/**
 * Created by Sarath Kumar on 01-12-2015.
 */

/*initializing DATABASE connection Through Sequelize Lib
 *Importing DATA MODEL from model folder to Sequelize
 * */
var DB_NAME = "ProjectPaygram";
var USER = "root";
var PASSWORD = "password";
var SequelizeLib = require('sequelize');
var sequelize;
sequelize = new SequelizeLib(DB_NAME, USER, PASSWORD,
    {
        host: 'localhost',
        dialect: 'mysql',
        debug:true,
        //logging: false

    });
var db = {};

//TABLE DEFINITION START
db.Users = sequelize.import(__dirname + '/Models/users.js');
db.paymentTokens = sequelize.import(__dirname + '/Models/paymentTokens.js');
db.token = sequelize.import(__dirname + '/Models/Sessiontoken.js');
db.payments = sequelize.import(__dirname + '/Models/payments.js');
db.Store = sequelize.import(__dirname + '/Models/Store.js');
db.Product = sequelize.import(__dirname + '/Models/Product.js');
db.banking_cards = sequelize.import(__dirname + '/Models/bankingCards.js');
db.gateway = sequelize.import(__dirname + '/Models/gateway.js');
db.API = sequelize.import(__dirname + '/Models/API.js');
//DEFINITION END


//RELATIONS START
db.payments.belongsTo(db.Users, {
    foreignKey: 'payee'
});
 db.payments.belongsTo(db.Users, {
    foreignKey: 'payer'
});

db.Users.hasMany(db.Store);
db.Users.hasMany(db.gateway);
db.Users.hasOne(db.API);
db.Users.hasMany(db.banking_cards);
db.Store.hasMany(db.Product);
db.Product.belongsTo(db.Store);
db.paymentTokens.belongsTo(db.Users);
db.paymentTokens.hasMany(db.payments);

//RELATIONS END

db.sequelize = sequelize;
db.SequelizeLib = SequelizeLib;
module.exports = db;
