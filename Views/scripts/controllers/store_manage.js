/**
 * Created by Sarath Kumar on 16-01-2016.
 */

app.controller("storeManage",function($scope,$http,$mdDialog,Evts,Socket){

    $scope.StoreType= [
        "Bookstore",
        "clothes store",
        "Grocery store",
        "Supermarket",
        "Bakery",
        "Delicatessen/deli",
        "Liquor store",
        "Drugstore",
        "Hardware store",
        "Nursery/garden center",
        "Newsstand",
        "Boutique",
        "Coffee shop",
        "gift shop",
        "sweet shop",
        "hypermarket",
        "Other"
    ] ;
    $scope.SelectStore=null;
    $scope.ImageUploadId=null;
    $scope.StoreModel= {};
    $scope.StoreModel.StoreTypes= null;
    $scope.StoreModel.StoreName= null;
    $scope.StoreModel.Desc= null;
    $scope.StoreModel.Address= null;
    $scope.StoreModel.OpenTime= null;
    $scope.StoreModel.CloseTime= null;
    $scope.error = true;
        $scope.Stores = [
        ];
        $http.get("/api/stores").then(function(data){
               $scope.Stores = data.data;
        },function(){

        });
    $scope.SelectedImag='';
    $scope.CroppedImage='';
    $scope.cropType="circle";
     $scope.AssignValue = function( Value){

        $scope.ImageUploadId = Value;
    }
    var handleFileSelect=function(evt)
    {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.SelectedImag=evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    $('#modal').on('show', function () {
        $(this).find('.modal-body').css({
            width:'auto', //probably not needed
            height:'auto', //probably not needed
            'max-height':'100%'
        });
    });


      $scope.UpdateStorePic = function(){
           $http.post("/api/Store/pic/"+$scope.ImageUploadId, JSON.stringify({bs64:$scope.CroppedImage}))
              .then(function(res)
              {
                   console.log(res.data)
                  console.log("IMAG UPDATED");
              },
              function(response){
                  console.log(response);

              } )
      };


      var StoreTokens = {}
  function getTOken(id,callback){
      $http.get("/api/Store/token/"+id).then(function(res){
      if(res.data){
          StoreTokens[res.data.StoreId] = res.data.TokenHASH;
          callback();
      }
      },function(){

      });
  }
    $scope.CreateStore = function(){
         if( $scope.StoreModel.StoreTypes==null || $scope.StoreModel.StoreName==null || $scope.StoreModel.Desc==null || $scope.StoreModel.Address==null){
             console.log(JSON.stringify($scope.StoreModel));
                 $scope.error = false;

         }  else{
             $scope.error = true;
             $http.post("/api/store/create", JSON.stringify($scope.StoreModel)).then(function(data){
                 $http.get("/api/stores").then(function(data){
                     $scope.Stores = data.data;
                 },function(){

                 });
             },function(error){
                            console.log(error)

             });

         }


    }
    $scope.deletStore = function(ev,storeid){
        var confirm = $mdDialog.confirm()
            .title('Would you like to delete this Store?')
            .ok('Please do it!')
            .cancel('Nop')
            .targetEvent(ev);

        $mdDialog.show(confirm).then(function() {
            $http.delete("/api/store/"+storeid).then(function(data){
                $http.get("/api/stores").then(function(data){
                    $scope.Stores = data.data;
                },function(){

                });
            },function(error){
                console.log(error)

            });
        }, function() {

        });
    }
     $scope.QRdialogue = function (ev,id)
     {

        $mdDialog.show({
            controller: DialogController,
            template: '<md-dialog aria-label="">\n\n    <div class="panel panel-card p"  style="width:1000px" ng-init="model.size=200" ng-init="qrdata=\'dsdsdsd\'">\n        <div class="row" >\n            <div class="col-sm-6">\n                <p>\n                    <span class="label">Customize</span>\n                </p>\n                <div class="md-form-group">\n                    <input colorpicker class="md-input" value="#000" ng-model="background" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>Background</label>\n                </div>\n                <div class="md-form-group">\n                    <input class="md-input" colorpicker value="#fff"   ng-model="foreground" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>ForeGround</label>\n                </div>\n                <p>\n                <div class="md-form-group">\n                    <input class="md-input"  max="450"  min="150" type="number" ng-model="model.size" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>Size</label>\n                </div>\n                </p>\n\n\n            </div>\n\n            <div class="col-sm-6 mbr" >\n                <qrcode  class="qrshadow" version="8" size="{{model.size}}" download bg="{{background}}"  fg="{{foreground}}"\n                         error-correction-level="HIGH" data="{{qrdata}}"></qrcode>\n                <p class="text-muted mbr">Click the QR code To Download</p>\n            </div>\n\n        </div>\n    </div>\n\n\n    <div class="md-actions" layout="row">\n        <span flex></span>\n        <md-button ng-click="hide()" class="md-primary">\n            Close\n        </md-button>\n    </div>\n</md-dialog>\n',
            targetEvent: ev
        })
         function DialogController($scope, $mdDialog) {
             $scope.hide = function () {
                 $mdDialog.hide();
             };
             if(StoreTokens[id]){
                 $scope.qrdata = StoreTokens[id] ;
             }   else{
                   getTOken(id,function(){
                       $scope.qrdata = StoreTokens[id] ;

                   });
             }


         }

    };
    $scope.NfcDialogue =function(ev,id){

        var confirm = $mdDialog.confirm()
            .title('Write NFC token')
            .content('You need to open your Merchant app and Stay online.nfc writer will be open up.\nNOTE:you can check you mobile app status below the navigation')
            .targetEvent(ev)
            .ok('Ok Ready to Write!')
            .cancel('Nop nop');

          if(StoreTokens[id]){
              $mdDialog.show(confirm).then(function() {
                var ob= {Token: StoreTokens[id]}
                  Socket.emit(Evts.EVT_WRITE_NFC,ob);
              }, function()
              {

              });
          } else{
             getTOken(id,function(){
                 $mdDialog.show(confirm).then(function() {
                     var token = {Token: StoreTokens[id]};
                     Socket.emit(Evts.EVT_WRITE_NFC,token);
                 }, function()
                 {

                 });
             })
          }

    }


});