/**
 * Created by Sarath Kumar on 10-01-2016.
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Product', {
        Name: {
            type: DataTypes.STRING,
            allowNull: false

        },
        Desc: {
            type:DataTypes.STRING(1000),
            allowNull: true
        },
        Price: {
            type:DataTypes.INTEGER,
            allowNull: false
        },
        productpic: {
            type:DataTypes.STRING,
            allowNull: true
        }
    });

};
