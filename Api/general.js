/**
 * Created by Sarath Kumar on 25-12-2015.
 */

var _ = require("underscore");
var crypto = require("crypto");

module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db, app);
    app.get("/api/emailcheck", function (req, res) {
        if (req.query.mail) {
            db.Users.findOne({where: {Email: req.query.mail}}
            ).then(function (user) {
                    if (user == null) {
                        res.sendStatus(404);

                    } else {
                        res.sendStatus(200);

                    }
                }, function () {
                    res.sendStatus(404);
                })
        }

    });
    app.get("/api/isAuthenticated", middleware.RequireAuth, function (req, res) {
        if (req.current_user) {
            res.json({
                UserType: req.UserType,
                userId: req.current_user.id
            })
        }

    });
    app.get("/rest/:page", middleware.RequireAuth, function (req, res) {
        var fs = require('fs');
        fs.readFile(process.cwd() + '/partial/' + req.params.page, function (err, data) {
            if (err) {
                res.sendStatus(404);

            } else {
                res.send(data);

            }
        });

    });
    app.get('/api/paytokens', middleware.RequireAuth, function (req, res) {
        var ID = req.current_user.id;
        db.Users.findById(ID).then(function (user) {
            db.paymentTokens.findAll({
                    where: {UserId: user.id},
                    attributes: {exclude: ['UserId']}
                }
            ).then(function (data) {
                    res.json(data)

                }, function (data) {
                    res.sendStatus(404);
                });

        }, function (data) {
            res.json(401)

        })

    });
    app.post('/api/token/delete', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'id');
        db.paymentTokens.findById(body.id).then(function (token) {
            if (token) {
                token.destroy().then(function () {
                    res.sendStatus(200);
                }, function () {
                    res.sendStatus(500);
                })
            } else {
                res.sendStatus(404);

            }

        }, function (data) {
            res.json(401)

        })

    });
    app.post('/api/token/desktoken/:id', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'DeskName');
        var StoreID = parseInt(req.params.id, 10);
        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: StoreID
            }
        }).then(function (Store) {

                if (Store) {

                    var DynamicToken = {
                        amount: 0,
                        type: "BOTH",
                        UseCounter: 0,
                        Tokentype: "Dynamic",
                        StoreId: StoreID,
                        DeskName: body.DeskName,
                        Desktoken: true,
                        Expire: 0
                    };

                    var randomString = crypto.randomBytes(32).toString('hex');
                    DynamicToken.TokenHASH = randomString;
                    db.paymentTokens.create(DynamicToken).then(function (dynamicPayIns) {

                        if (dynamicPayIns) {
                            res.json(dynamicPayIns);

                        } else {
                            res.sendStatus(204);

                        }

                    }, function (error) {
                        console.log(error);

                        res.sendStatus(401);
                    });

                } else {

                    res.sendStatus(404);
                }
            }
            , function () {
                res.sendStatus(401);

            });


    });
    app.get('/api/token/desktoken/:id', middleware.RequireAuth, function (req, res) {
        var StoreID = parseInt(req.params.id, 10);
        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: StoreID
            }
        }).then(function (Store) {
                if (Store) {

                    db.paymentTokens.findAll(
                        {
                            where: {
                                type: "BOTH",
                                Tokentype: "Dynamic",
                                StoreId: StoreID,
                                Desktoken: true
                            }
                        }
                    ).then(function (dynamicPayIns) {
                            if (dynamicPayIns) {
                                res.json(dynamicPayIns);
                            } else {
                                res.sendStatus(204);

                            }
                        }, function (error) {
                            console.log(error)
                            res.sendStatus(401);
                        });

                } else {

                    res.sendStatus(404);
                }
            }
            , function (error) {

                res.sendStatus(401);

            });

    });
    app.delete('/api/token/:id', middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.paymentTokens.findById(ID).then(function (token) {
            if (token) {
                token.destroy().then(function () {
                    res.sendStatus(201);
                }, function () {
                    res.sendStatus(500);
                });
            } else {
                res.sendStatus(404);

            }
        }, function () {
            res.sendStatus(401);

        })
    });
    app.get('/api/payments/', middleware.RequireAuth, function (req, res) {
        if (req.UserType == "Customer") {
            db.sequelize.query('SELECT payments.* ,users.* FROM `projectpaygram`.`payments`  INNER  JOIN users  ON payments.payee=users.id ',
                {model: db.payments}
            ).then(function (payments) {

                    res.json(payments);
                }, function (error) {
                    res.json(error);
                })
        } else if (req.UserType == "Merchant") {
            db.sequelize.query(
                'SELECT payments.* ,payee.Name as PayeeName,payee.Email AS PayeeEmail ,payee.id AS PayeeId,payee.profilepic AS PayeeProfile ' +
                ' FROM `projectpaygram`.`payments`  INNER  JOIN users AS payee  ON payments.payee=payee.id ',
                {model: db.payments}
            ).then(function (payments) {
                    res.json(payments);
                }, function (error) {
                    res.json(error);
                })
        }
    });
    app.get('/api/dashboard/', middleware.RequireAuth, function (req, res) {
        var user = req.current_user.id;
        db.payments.findAll({
            where: {
                payee: user
            }
        }).then(function (data) {
            res.json(data);
        }, function (data) {
            res.json(data);

        });
    });
    app.get("/api/payment_cards", middleware.RequireAuth, function (req, res) {
        var user = req.current_user.id;
        db.banking_cards.findAll({
            where: {
                UserId: user
            }
        }).then(function (data) {
            if (data == null) {
                res.sendStatus(400);
            }
            res.json(data);

        }, function (error) {
            res.json(error);

        })
    })
    app.post("/api/payment_cards", middleware.RequireAuth, function (req, res) {
        var user = req.current_user.id;

        var body = _.pick(req.body, 'card_number', 'expire_month', 'expire_year', 'first_name', 'last_name', 'card_type');

        body.UserId = user;
        db.banking_cards.create(body).then(function (data) {
            res.json(data);

        }, function (error) {
            res.json(error);

        })
    });

    app.post("/api/paymentCallback", function (req, res) {
        console.log("Payment Callback");

        var body = req.body;
        var userid = body.uid;
        console.log(body)

        var responses = {
            pay_id: crypto.randomBytes(6),
            create_time: body.create_time,
            amount: body.amount,
            currency: "INR",
            card_number: body.card_no,
            name: body.name,
            state: body.state,
            UserId: body.uid
        };
        db.Users.findById(userid).then(function (user) {
            var userbalnce = parseInt(user.Balance);
            var amount = parseInt(body.amount);


            var net = userbalnce + amount;
            console.log(net);


            user.update({
                Balance: net
            }).then(function () {
                res.sendStatus(200);
            });


        }, function (error) {
            res.json(error);

        })


    })


};