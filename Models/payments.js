/**
 * Created by Sarath Kumar on 09-12-2015.
 */


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('payments', {
            amount: {
                type: DataTypes.INTEGER,
                allowNull: false,
                validate: {
                    isNumeric: true,
                    min: 1,
                    max: 50000
                }
            },
            paymentType: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    isIn: [['NFC', 'QR', 'BOTH']]
                }
            },
            method: {
                type: DataTypes.STRING,
                allowNull: true

            },
            TokenType: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    isIn: [['Static', 'Donation', 'Dynamic']]
                }
            }

        }
    )
        ;

}

;
