/**
 * Created by Sarath Kumar on 01-12-2015.
 */
/*Data model For cutomers(personal users);*/
var bcrypt = require('bcryptjs');
var _ = require('underscore');
var crypto = require("crypto-js/aes");
var JWT = require("jsonwebtoken");

module.exports = function (sequelize, DataTypes) {
    var Users= sequelize.define('Users', {
            Name: {
                type: DataTypes.STRING,
                allowNull: false
            },

            Email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                validate: {
                    isLowercase: true,
                    isEmail: true,
                 }


            },
            isMerchant: {
                type: DataTypes.BOOLEAN,
                defaultValue: false

            },
            Balance: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
                validate: {
                    isNumeric: true,
                }
            },
            password: {
                type: DataTypes.VIRTUAL,
                allowNull: false,
                validate: {
                    len: [7, 100]
                },
                set:function(value){
                    var salt = bcrypt.genSaltSync(10);
                    var hashedPassword = bcrypt.hashSync(value, salt);
                    this.setDataValue('password', value);
                    this.setDataValue('salt', salt);
                    this.setDataValue('password_hash', hashedPassword);
                }
            },
            salt: {
                type: DataTypes.STRING
            },
            password_hash: {
                type: DataTypes.STRING
            },
            Country: {
                type: DataTypes.STRING,
                allowNull: false

            },
            State: {
                type: DataTypes.STRING,
                allowNull: false

            },
            zip: {
                type: DataTypes.INTEGER,
                allowNull: false

            },
            accConfirm: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false

            },
            address: {
                type: DataTypes.STRING(500),
                allowNull: false

            },
            profilepic: {
                type: DataTypes.STRING(150),
                allowNull: true,
                unique: true

            },
            Merchant_category: {
                type: DataTypes.STRING,
                allowNull: true
            }

        },
        {
            hooks: {
                beforeValidate: function (user, options) {
                    if (typeof  user.Email === "string") {
                        user.Email = user.Email.toLowerCase();
                    }
                }
            },
            instanceMethods:{
                    toPublicJSON: function (Is_merchant) {
                        if(Is_merchant=true) {

                        }
                         return _.pick(this.toJSON(), 'id', 'Name', 'Email', 'address',  'Balance', 'profilepic', 'accConfirm', 'Country', 'State', 'zip', 'createdAt', 'updatedAt');
                     }  ,
                    generateJWT:function(payload){

                        try {
                            var stringData = JSON.stringify({
                                id: this.get('id'),
                                payload: payload
                            });
                            var encryptedData = crypto.encrypt(stringData, 'abc123!@#!').toString();
                            return JWT.sign({
                                token: encryptedData
                            }, 'qwerty098');
                        } catch (e) {
                            console.error(e);
                            return undefined;
                        }

                    }
                }  ,
            classMethods:{
                Authenticate:function(body,isMerchant){
                    return new Promise(function(resolve,reject){
                        var where;
                       if(typeof  body.Email !=="string" || typeof body.password !=="string" ){

                           reject("Invalid STRING");
                       }
                        if(isMerchant){
                            where ={
                                Email: body.Email,
                                isMerchant:true
                            }
                        }else{
                            where ={
                                Email: body.Email,
                                isMerchant:false
                            }
                        }
                        Users.findOne({
                            where:where
                        }).then(function(customer){
                               if(!customer || !bcrypt.compareSync(body.password,customer.get('password_hash'))){
                                reject("Invalid CREDENT");
                              }
                              resolve(customer);
                        })
                    });
                }
            }
        }

    );
    return Users;
};