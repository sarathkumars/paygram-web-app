/**
 * Created by Sarath Kumar on 10-04-2016.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var _ = require('underscore');
var request = require('request');
app.set('view engine', 'ejs');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public/'));
var Pay_ids = [];
app.get('/', function (req, res) {
    try {
        var dataparms = {}
        //_.pick(req.body, 'uid', 'card_no','name', 'amount', 'card_issue', 'sec_code', 'expiry')
    }
    catch (Ex) {
        res.sent("BAD PARAMS");
        return;
    }
    //Pay_ids.push("id",)
    res.render('payview', {
        data: dataparms
    });
});

app.post('/', function (req, res) {
    try {
        var token = req.query.authkey;
        var dataparms =
            _.pick(req.body, 'uid', 'card_no', 'name', 'amount', 'card_issue', 'sec_code', 'expiry', 'callback');
            dataparms.key = token;
    }
    catch (Ex) {
        res.sent("BAD PARAMS");
        return;
    }
    Pay_ids.push(dataparms);
    res.render('payview', {
        data: dataparms
    });
});

app.post('/payprocess', function (req, res) {
    try { 
        var dataparms = _.pick(req.body, 'uid', 'card_no', 'name', 'amount', 'card_issue', 'sec_code', 'expiry');
        console.log(req.body);
        var obj = _.where(Pay_ids, function (data) {
            return data.uid == dataparms.uid;
        });
         Pay_ids=[];
        obj= obj[0];
        obj.currency = "INR";
        obj.state = "SUCCESS";
        console.log(obj);
        obj.create_time = new Date().getTime();
         var options = {
            method: 'POST',
            uri: "http://192.168.1.33:4000/api/paymentCallback",
            headers: {
                'AcNtkn': obj.key,
                'cache-control': 'no-cache',
                'content-type': 'application/json',
            
             },
            body: obj,
            json: true
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
           console.log("SUCCESS",response.status);
            res.json({status: "success"})

        });


    }
    catch (Ex) {
        console.log(Ex)

    }

});
app.post('/test', function (req, res) {
    // console.log(req.body);
    res.sendStatus(200)

});
app.listen(5500);
