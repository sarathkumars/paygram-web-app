/**
 * Created by Sarath Kumar on 07-02-2016.
 */

var Evnts = require("./../Socket/SocketEvnts.js");
var _ = require("underscore");
var ConnectionMngr = require("./../Utils/ConnectionManager.js");
var DeskService = require("./../Utils/DeskConnectionsManger.js");
var moment = require("moment");
module.exports = function (socket, db, io) {

    socket.on(Evnts.LAUNCH_DESK, function (data) {
        if (ConnectionMngr.GetUserBySocket(socket, ConnectionMngr.WEBAPP))
        {

            var Roomdata = {
                Storeid: data.storeID,
                host: socket
            };
            DeskService.AddRoom(Roomdata);
            socket.emit(Evnts.EVT_STATES, {state: "DESK", payload: true})
        }
    });
    socket.on(Evnts.EVT_JOIN, function (data) {
        var storeID = data.StoreID;
        var room = io.sockets.in(storeID);
        if (DeskService.CheckOnline(storeID)) {
            var clientStatus = DeskService.AddClient(storeID, {socketId: socket, token: data.tokenID});
            if (clientStatus == true)
            {
                console.log("HITS");
                socket.emit(Evnts.EVT_JOIN_REPLY, {payload: true, status: "success"});
                socket.join(room);
                DeskService.GetHost(storeID).emit(Evnts.EVT_JOIN_REPLY, data);
            }
            else if (clientStatus == "RESERVED") {
                socket.emit(Evnts.EVT_JOIN_REPLY, {payload: false, status: "reserved"});
            }
        } else {
            socket.emit(Evnts.EVT_JOIN_REPLY, {payload: false, status: "offline"});

        }

    });
    socket.on(Evnts.EVT_ORDER, function (data) {
        console.log(data);
        var host = DeskService.GetHost(data.storeID);
        host.emit(Evnts.EVT_ORDER, data);


    });
    socket.on(Evnts.EVT_ORDER_REPLY, function (data) {
        console.log(data);
        var client = DeskService.GetClient(data.tokenid);
        if(data.accept==true) {
            client.socket.emit(Evnts.EVT_ORDER_REPLY,
                {ID:data.productID,
                    orderStatus:"SUCCESS"
                })
        }else{
            client.socket.emit(Evnts.EVT_ORDER_REPLY,{
                ID:data.productID,
                orderStatus:"FAIL"})

        }

    } );
    socket.on(Evnts.EVT_LEAVE, function (data) {
        console.log(data);
        var host = DeskService.GetHost(data.storeID);
        host.emit(Evnts.EVT_LEAVE, data);
        console.log(DeskService.RemoveClient(data.tokenID));
        socket.emit(Evnts.EVT_LEAVE, {payload: true, status: "success"});

    });
    socket.on(Evnts.RE_INIT_STORE, function (data) {

        if (DeskService.CheckOnline(data.storeID)) {
            var Roomdata = {
                Storeid: data.storeID,
                host: socket
            };
            DeskService.AddRoom(Roomdata);
            socket.emit(Evnts.EVT_STATES, {state: "DESK", type: "INSTANCE"});

        } else {
            var Roomdata = {
                Storeid: data.storeID,
                host: socket
            };
            DeskService.AddRoom(Roomdata);
            socket.emit(Evnts.EVT_STATES, {state: "DESK", type: "RE_INIT",})
        }

    });

};