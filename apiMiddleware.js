/**
 * Created by Sarath Kumar on 18-04-2016.
 */
var _ = require("underscore");
module.exports = function (db, app) {
    return {
        ApiAuth: function (req, res, next) {
            var headers = req.headers;
            if (_.has(headers, "apikey")) {
                var key = headers.apikey;
                db.API.findOne(
                    {
                        where: {
                            key: key
                        }
                    }).then(function (data) {

                        if (data == null) {
                            res.status(400).json({"Error": "you must Provide a valid API key"});
                        } else {
                            req.api = data;
                            req.current_user = data.UserId;
                            next();

                            console.log("came here");

                        }

                    }, function () {
                        res.status(500).json({"Error": ""});

                    });


            } else {
                res.status(400).json({"Error": "you must Provide API key"});
            }


        }

    }
};
