var express = require("express");
 var PORT = 4000;
var app = express();
var _ = require("underscore");
var crypto = require("crypto-js");
var http = require("http").Server(app);
var db = require('./db.js');
var multer = require('multer');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(cookieParser());
//DEFINE STORAGE FOR PROFILE PIC(Multer LIB COnfig)
var storage = multer.diskStorage(
    {
        destination: function (req, file, cb) {
            cb(null, __dirname + '/public/profile/');
        },
        filename: function (req, file, cb) {
             var extension = file.originalname.substr(file.originalname.length - 3);
            cb(null, crypto.MD5("profile_" + Date.now()).toString()+ "." + extension);
        }
    });

var upload = multer({
        storage: storage,
        limits: {
            fileSize: 10000000,
            files: 1,
            fields: 1
        }
    }
).single('avatar');
require("./Socket/SocketController")(http,db);
//
//app.use(function (err, req, res, next) {
//    console.log(err)
//    res.sendStatus(500);
//});
//LOADING  API INTO THE EXPRESS
require("./Api/customer.js")(app, db, upload);
require("./Api/merchant.js")(app, db, upload);
require("./Api/staticPay.js")(app, db, upload);
require("./Api/dynamicPay.js")(app, db, upload);
require("./Api/donatepay.js")(app, db, upload);
require("./Api/general.js")(app, db, upload);
require("./Api/api.js")(app, db, upload);
require("./Api/Store.js")(app, db, upload);
require("./Api/Product.js")(app, db, upload);

var middleware = require("./middleware.js")(db,app);
app.use(express.static(__dirname + '/public/libs'));
app.use(express.static(__dirname + '/views/'));
app.use(express.static(__dirname + '/public/'));
  //app.use('/',express.static(__dirname + '/views/public/'));
//app.use('/merchant',middleware.CheckMerchant, express.static(__dirname+'/views/merchant'));
//

//INIT DATABASE CONNECTION AND THEN START THE SERVER
db.sequelize.sync(
    //{force:true}
).then(function () {
        http.listen(PORT, function () {
        console.log("SERVER RUNNING ON port:" +PORT);
            var os = require('os');
            var ifaces = os.networkInterfaces();
             //console.log(ifaces.Ethernet[1].address)

        });
}, function (error) {
            console.error("ERROR WHILE CONNECTING DB") ;
            console.error(error);
});
