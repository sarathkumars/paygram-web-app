/**
 * Created by Sarath Kumar on 01-12-2015.
 */
var _ = require("underscore");
module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db, app);

/**DEPRECATION**/
 // READ ALL merchants From DB
    //app.get('/api/merchant', middleware.RequireAuth, function (req, res) {
//console.log("FUCKED");

    //    db.Users.findAll({
    //        attributes: ['Name', 'id', 'address', 'Country','Email', 'profilepic'],
    //        where: {
    //            isMerchant: true
    //        }
    //    }).then(function (merchants) {
    //        if (merchants.length > 0) {
    //            res.json(merchants)
    //        }
    //        else {
    //            res.sendStatus(200);
    //        }
    //    }, function (error) {
    //        res.status(400).json(error);
    //
    //    });
    //});
    // GET  || READ merchant From DB With  Id
    // POST || CREATE merchant
    //GET CURRENT LOGGED IN MERCHANT

    app.get('/api/merchant/', middleware.RequireMerchant, function (req, res) {
        var ID = req.current_user.id;
        db.Users.findById(ID).then(function (merchants) {

              res.json(merchants);

        }, function (error) {
            res.status(400).json(error);
        });

    });

    app.post('/api/merchant', function (req, res)
    {
        var body = _.pick(req.body, 'Name', 'Email', 'Country', 'Merchant_category', 'State', 'zip', 'address', 'password');
        body.isMerchant = true;
        db.Users.create(body).then(function (merchant) {
            res.json(merchant.toPublicJSON(false));
        }, function (error) {
            res.status(400).json(error);

        });

    });
    //POST || PROFILE PIC UPLOAD using Multer
    app.post('/api/merchant/upload/', function (req, res) {
        var ID = req.current_user.id;
        console.log(req.body.bs64);

         if (req.body.bs64 != undefined)
         {
            var base64Data = req.body.bs64.replace(/^data:image\/png;base64,/, "");
            var filename = require("crypto-js").MD5("profile_" + Date.now()).toString() + ".png";
            require("fs").writeFile(process.cwd() + "/public/profile/" + filename, base64Data, 'base64', function (err) {
                if (err) {
                    res.status(500).send();
                } else {
                    attributes = {profilepic: filename};
                    db.Users.findById(ID).then(function (merchant) {
                        if (merchant) {
                            merchant.update(attributes).then(function (merchant) {
                                res.json(merchant.toPublicJSON());
                            }, function (e) {
                                res.status(400).json(e);
                            });
                        } else {
                            res.status(404).send();
                        }
                    }, function () {
                        res.status(500).send();
                    });
                }
            });
        } else {
            upload(req, res, function (err) {
                if (err) {
                    return res.send(req.file);
                } else {
                    attributes = {profilepic: req.file.filename};
                    db.Users.findById(ID).then(function (merchant) {
                        if (merchant) {
                            merchant.update(attributes).then(function (merchant) {
                                res.json(merchant.toPublicJSON());
                            }, function (e) {
                                res.status(400).json(e);
                            });
                        } else {
                            res.status(404).send();
                        }
                    }, function () {
                        res.status(500).send();
                    });
                }
            });
        }
    });
    // DELETE merchant From DB
    app.delete('/api/merchant/', middleware.RequireMerchant, function (req, res) {
        var ID = req.current_user.id;
        db.Users.destroy({
            where: {
                id: ID,
                isMerchant: true

            }
        }).then(function (rowsDeleted) {
            if (rowsDeleted === 0) {
                res.status(404).json({
                    error: 'No merchant with id :' + ID
                });
            } else {
                res.status(204).send();
            }
        }, function () {
            res.status(500).send();
        });


    });
    // PUT ||Update merchant From DB
    app.put('/api/merchant/', middleware.RequireMerchant, function (req, res) {
        var ID = req.current_user.id;
        var body = _.pick(req.body, 'Name', 'Email', 'Merchant_category', 'Country', 'State', 'zip', 'address', 'password');
        var attributes = {};
        if (body.hasOwnProperty('Name')) {
            attributes.Name = body.Name;
        }
        if (body.hasOwnProperty('password')) {
            attributes.password = body.password;
        }
        if (body.hasOwnProperty('Email')) {
            attributes.Email = body.Email;
        }
        if (body.hasOwnProperty('Country')) {
            attributes.Country = body.Country;
        }

        if (body.hasOwnProperty('State')) {
            attributes.State = body.State;
        }
        if (body.hasOwnProperty('zip')) {
            attributes.zip = body.zip;
        }
        if (body.hasOwnProperty('address')) {
            attributes.zip = body.zip;
        }
         db.Users.findById(ID).then(function (merchant) {
            if (merchant) {
                merchant.update(attributes).then(function (merchant) {
                    res.json(merchant.toPublicJSON());
                }, function (e) {
                    res.status(400).json(e);
                });
            } else {
                res.status(404).send();
            }
        }, function () {
            res.status(500).send();
        });
    });
    // delete ||Log out
    app.post('/api/merchant/logOut', middleware.RequireMerchant, function (req, res) {
        req.token.destroy().then(function () {
            res.clearCookie('AcNtknCK', {path: '/'});
            res.sendStatus(200);
        });
    });

    app.post('/api/merchant/login', function (req, res) {
         var body = _.pick(req.body, 'Email', 'password');
        db.Users.Authenticate(body, true).then(function (merchant) {
            var token = merchant.generateJWT({
                id: merchant.get('id'),
                type: "merchant"
            });
            db.token.create({
                token: token
            }).then(function (tokenInstance) {
                res.cookie('AcNtknCK', tokenInstance.get("token"));
                res.append("AcNtkn", tokenInstance.get("token"));
                 res.json(merchant.toPublicJSON());
            }, function () {
                res.sendStatus(401);
            });
        }, function (error) {
            console.log(error);
            res.sendStatus(401);
        });

    });


};


