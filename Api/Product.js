/**
 * Created by Sarath Kumar on 11-01-2016.
 */
var _ = require("underscore");
module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db, app);

    app.post("/api/product/create/:storeID", middleware.RequireMerchant, function (req, res) {
        var body = _.pick(req.body, 'Name', 'Desc', 'Price');
        var StoreID = parseInt(req.params.storeID, 10);
        db.Store.find({
            where: {
                UserId: req.current_user.id,
                id: StoreID
            }}).then(function(Store){
            if(Store!=null){
                body.StoreId =Store.id;
                db.Product.create(body).then(function (Store) {
                    res.json(Store);
                }, function (error) {
                    res.status(400).json(error);
                });
            } else{
                res.sendStatus(400);

            }

        },function(error){
            res.status(400).json(error);

        });

    });

    app.delete("/api/product/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Product.find({
            where: {
                 id: ID
            }
        }).then(function (Product) {
            db.Store.find({
                where:{
                    id:Product.StoreId,
                    UserId:req.current_user.id
                }
            }).then(function(data){
                if(data!=null){
                    Product.destroy().then(function (rowsDeleted) {
                        if (rowsDeleted === 0) {
                            res.sendStatus(500)

                        } else {
                            res.sendStatus(204);
                        }

                    }, function (error) {
                        res.status(500).json(error)
                    })
                }else{
                    res.status(400).json({error: "DENIED"})

                }

            },function(){
                res.sendStatus(404).json({error: "NOT FOUND"})

            }) ;

        }, function () {
            res.sendStatus(404).json({error: "NOT FOUND"})

        });


    });

    app.put("/api/product/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        var body = _.pick(req.body, 'Name', 'Desc', 'Price');

        db.Product.find({
            where: {
                id: ID
            }
        }).then(function (Product) {
            if(Product!=null){
                db.Store.find({
                    where: {
                        id: Product.StoreId,
                        UserId: req.current_user.id
                    }
                }).then(function (data) {

                    if(data!=null){
                        Product.update(body).then(function (product) {
                            res.json(product);
                        }, function () {
                            res.sendStatus(400);

                        });
                    }else{
                        res.sendStatus(400);

                    }

                }, function (error) {
                    res.sendStatus(400);

                });
            }else{
                res.sendStatus(404);
            }

        }, function () {
            res.sendStatus(404);

        });
    });

    app.get("/api/products/:storeId", middleware.RequireAuth, function (req, res) {
        var StoreID = parseInt(req.params.storeId, 10);
       var attributes;
        if(req.UserType==="Customer"){
          attributes=["id","Name","Desc","Price","StoreId","productpic"]
        } else{
            attributes=["id","Name","Desc","Price","StoreId","createdAt","productpic","updatedAt"]
        }
        db.Product.findAll({
            attributes: attributes,
            where: {
                 StoreId:StoreID
            }
        }).then(function (products) {
            res.json(products)
        }, function (error) {
            res.json(404)
        });

    });

    app.get("/api/product/:id", middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Product.findOne({where:{Id:ID}}).then(function (store) {
            res.json(store);
        }, function (error) {
            res.sendStatus(404);
        });
    });

    app.post("/api/product/pic/:id", middleware.RequireMerchant, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        try{
            if (req.body.bs64 != undefined) {
                db.Product.findById(ID).then(function(Product){
                    db.Store.find({
                        where: {
                            UserId: req.current_user.id,
                            id: Product.StoreId
                        }
                    }).then(function (Store) {
                        if (Store != null) {
                            var base64Data = req.body.bs64.replace(/^data:image\/png;base64,/, "");
                            var filename = require("crypto-js").MD5("profile_" + Date.now()).toString() + ".png";
                            require("fs").writeFile(process.cwd() + "/public/product/" + filename, base64Data, 'base64', function (err) {
                                if (err) {

                                    res.status(500).send();
                                } else {
                                    var attributes = {productpic: filename};
                                    Product.update(attributes).then(function (Product) {
                                        res.json(Product);
                                    }, function () {

                                        res.sendStatus(500);
                                    });

                                }
                            });
                        }
                    }, function () {
                        res.sendStatus(401);

                    });
                },function(error){
                    res.sendStatus(404);

                })

            }
        } catch(err){
            res.sendStatus(500);
        }
    })
    app.get("/api/product/findall",middleware.RequireAuth,function(req,res){

    });
};
