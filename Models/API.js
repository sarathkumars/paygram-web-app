/**
 * Created by Sarath Kumar on 14-04-2016.
 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('API_keys',
        {
            Key: {
                type: DataTypes.STRING,
                allowNull: false
            }
        }
    );
};
