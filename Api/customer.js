/**
 * Created by Sarath Kumar on 01-12-2015.
 */
var _ = require("underscore");
module.exports = function (app, db, upload) {
    var middleware = require("./../middleware.js")(db,app);
    // READ ALL Customers From DB
    app.get('/api/customers/', middleware.RequireAuth, function (req, res) {
         db.Users.findAll({
            attributes: ['Name', 'Id', 'address', 'Country', 'profilepic'],
            where: {
                isMerchant: false
            }
        }).then(function (customers) {
             if (customers.length > 0) {
                res.json(customers)
            }
            else {
                res.sendStatus(200);
            }
        }, function (error) {
            res.status(400).json(error);

        });
    });
    // GET  || READ Customer From DB With  Id
    app.get('/api/customer/', middleware.RequireAuth, function (req, res) {
         var ID = req.current_user.id;
         db.Users.findById(ID).then(function (user)
         {
             res.json(user);
        }, function (error) {
            res.status(400).json(error);
        });

    });
    app.get('/api/customer/:id', middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
         db.Users.findById(ID).then(function (user)
         {
             var picked = _.pick(user,'Name','Email','Country','State','zip','profilepic','id');
             console.log(picked)

              res.json(picked);
        }, function (error) {
            res.status(400).json(error);
        });

    });
    // POST || CREATE Customer
    app.post('/api/customer', function (req, res) {
        var body = _.pick(req.body, 'Name', 'Email', 'Country', 'State', 'zip', 'address', 'password');
        db.Users.create(body).then(function (customer) {
            res.json(customer.toPublicJSON(false));
        }, function (error) {
            res.status(400).json(error);

        });

    });
    //POST || PROFILE PIC UPLOAD using Multer
    app.post('/api/customer/upload/:id', middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
         if (req.body.bs64 != undefined)
        {
            var base64Data = req.body.bs64.replace(/^data:image\/png;base64,/, "");
            var filename = require("crypto-js").MD5("profile_" + Date.now()).toString() + ".png";
            require("fs").writeFile(process.cwd() + "/public/profile/" + filename, base64Data, 'base64', function (err) {
                if (err) {
                    res.status(500).send();
                } else {
                    attributes = {profilepic: filename};
                    db.Users.findById(ID).then(function (merchant) {
                        if (merchant) {
                            merchant.update(attributes).then(function (merchant) {
                                res.json(merchant.toPublicJSON());
                            }, function (e) {
                                res.status(400).json(e);
                            });
                        } else {
                            res.status(404).send();
                        }
                    }, function () {
                        res.status(500).send();
                    });
                }
            });
        } else {
            upload(req, res, function (err) {
                if (err) {
                    return res.send(req.file);
                } else {
                    attributes = {profilepic: req.file.filename};
                    db.Users.findById(ID).then(function (merchant) {
                        if (merchant) {
                            merchant.update(attributes).then(function (merchant) {
                                res.json(merchant.toPublicJSON());
                            }, function (e) {
                                res.status(400).json(e);
                            });
                        } else {
                            res.status(404).send();
                        }
                    }, function () {
                        res.status(500).send();
                    });
                }
            });
        }
    });
    // DELETE Customer From DB
    app.delete('/api/customer/:id', middleware.RequireCustomer, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        db.Users.destroy({
            where: {
                id: ID,
                isMerchant: false
            }
        }).then(function (rowsDeleted) {
            if (rowsDeleted === 0) {
                res.status(404).json({
                    error: 'No Customer with id :' + ID
                });
            } else {
                res.status(204).send();
            }
        }, function () {
            res.status(500).send();
        });


    });
    // PUT ||Update Customer From DB
    app.put('/api/customer/:id', middleware.RequireAuth, function (req, res) {
        var ID = parseInt(req.params.id, 10);
        var body = _.pick(req.body, 'Name', 'Email', 'Country', 'State', 'zip', 'address', 'password');
        var attributes = {};
        if (body.hasOwnProperty('Name')) {
            attributes.Name = body.Name;
        }
        if (body.hasOwnProperty('password')) {
            attributes.password = body.password;
        }
        if (body.hasOwnProperty('Email')) {
            attributes.Email = body.Email;
        }
        if (body.hasOwnProperty('Country')) {
            attributes.Country = body.Country;
        }

        if (body.hasOwnProperty('State')) {
            attributes.State = body.State;
        }
        if (body.hasOwnProperty('zip')) {
            attributes.zip = body.zip;
        }
        if (body.hasOwnProperty('address')) {
            attributes.zip = body.zip;
        }
         db.Users.findById(ID).then(function (customer) {
            if (customer) {
                customer.update(attributes).then(function (customer) {
                    res.json(customer.toPublicJSON());
                }, function (e) {
                    res.status(400).json(e);
                });
            } else {
                res.status(404).send();
            }
        }, function () {
            res.status(500).send();
        });
    });
    // delete ||Log out
    app.post('/api/customer/logOut', middleware.RequireAuth, function (req, res) {
        req.token.destroy().then(function () {
            res.clearCookie('AcNtknCK', { path: '/' });
            res.clearCookie('AcNtknCK', { path: '/api/customer' });
            res.sendStatus(200);
        });

    });
    app.post('/api/customer/login', function (req, res) {
        var body = _.pick(req.body, 'Email', 'password');
        db.Users.Authenticate(body,false).then(function (customer) {
            var token = customer.generateJWT({
                id: customer.get('id'),
                type: "customer"
            });
            db.token.create({
                token: token
            }).then(function (tokenInstance) {
                res.cookie('AcNtknCK', tokenInstance.get("token"));
                res.append("AcNtkn", tokenInstance.get("token"));
                 res.json(customer.toPublicJSON());
            }, function () {
                res.sendStatus(401);
            });
        }, function (error) {
            console.log(error);
            res.sendStatus(401);
        });

    });

};


