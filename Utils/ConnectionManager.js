/**
 * Created by Sarath Kumar on 12-01-2016.
 */

var _ = require("underscore");
var loki = require('lokijs');
LokiDB = new loki('Users_Online.json');
var WEB_CONNECTIONS = LokiDB.addCollection("WEB");
var MOB_CONNECTIONS = LokiDB.addCollection("MOB");

module.exports = {
    CONNECTIONS_WEB: {},
    CONNECTIONS_MOB: {},
    MOBILE: "MOB",
    WEBAPP: "WAP",

    /**
     * @return {boolean}
     */
    AddConnection: function (User, Socket, Apptype) {

        if (Apptype == this.WEBAPP) {

            WEB_CONNECTIONS.insert({
                Socket: Socket,
                User: User
            });

            this.OnlineCheck(User.id);
            console.log("USER ->"+User.Name+" -> CONNECTED FROM WEB");

        } else {
            if (Apptype === this.MOBILE) {
                MOB_CONNECTIONS.insert({
                    Socket: Socket,
                    User: User
                });
                console.log("USER ->"+User.Name+" -> CONNECTED FROM MOB");

                 this.OnlineCheck(User.id);

                return true;
            }
        }

    },
    RemoveConnection: function (Socket) {
        if (MOB_CONNECTIONS.where(function (obj) {
                return _.isEqual(obj.Socket, Socket)
            }).length != 0) {
            MOB_CONNECTIONS.removeWhere(function (obj) {
                return _.isEqual(obj.Socket, Socket)


            });
             console.log("Socket disconnected  ->"+Socket.id+" ->  FROM MOB");

        } else if (WEB_CONNECTIONS.where(function (obj) {
                return _.isEqual(obj.Socket, Socket)

            })
        ) {
            WEB_CONNECTIONS.removeWhere(function (obj) {
                return _.isEqual(obj.Socket, Socket)
            });
             console.log("Socket disconnected  ->"+Socket.id+" ->  FROM WEB");


        }
    },
    /**
     * @return {boolean}
     */
    GetSocketByUserid: function (id, where) {
        var Connection;
        if (where === this.MOBILE) {

            Connection = MOB_CONNECTIONS.where(function (obj) {
                return obj.User.id == id
            });

            if (Connection!=undefined && Connection.length!=0)
            {

                return Connection[0].Socket;
            } else {
                return false;
            }

        } else if (where === this.WEBAPP) {

            Connection = WEB_CONNECTIONS.where(function (obj) {
                return obj.User.id == id;
            });
            if (Connection!=undefined && Connection.length!=0)
            {
                return Connection[0].Socket;
            } else {
                return false;
            }

        }


    },
    /**
     * @return {boolean}
     */
    GetUserBySocket: function (socket, where) {
        if (where === this.MOBILE)
          {
            var Connection = MOB_CONNECTIONS.where(function (obj) {
                return _.isEqual(obj.Socket, socket)
            });

              if (Connection!=undefined && Connection.length!=0)
              {
                  return Connection[0].User;

              }  else{
                  return false;
              }

        } else if (where === this.WEBAPP) {
            var Connection = WEB_CONNECTIONS.where(function (obj) {
                return _.isEqual(obj.Socket, socket)
            });
            if (Connection!=undefined && Connection.length!=0)
            {
                return Connection[0].User;

            }  else{
                return false;
            }

        }

    },
    OnlineCheck: function (userId) {
        var Online = {};
        Online.WEB = !!this.GetSocketByUserid(userId, this.WEBAPP);
        Online.MOB = !!this.GetSocketByUserid(userId, this.MOBILE);
        console.log(Online);
        return Online;
    }
};


