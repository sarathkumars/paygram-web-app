/**
 * Created by Sarath Kumar on 03-01-2016.
 */
'use strict';

/* Controllers */
app.controller('GeneratorCtrl', function ($scope, $http, $mdToast, $mdDialog, Auth,Socket,Evts) {
    $scope.schems = ['Static', 'Dynamic', 'Donation'];
    $scope.cardctrl = {};
    $scope.cardctrl.data = "PayGram QR"
    $scope.cardctrl.visible = false;
    $scope.tokenmodel={}
    $scope.tokenmodel.nfc = null;
    $scope.tokenmodel.qr = null;
    $scope.tokenmodel.expire = 1;
    $scope.showopt = false;
    $scope.token = {};
    $scope.token.type = "";
    $scope.token.Amount = null;
    $scope.cardctrl.showQR = false;
    $scope.cardctrl.showNFC = false;
    $scope.token.expire = null;
    $scope.token.scheme = null;
    $scope.cardctrl.progress = false;

     var hash = "Paygram"
    $scope.typealter = function () {
        if ($scope.tokenmodel.nfc == true && $scope.tokenmodel.qr == true) {
            $scope.token.type = "BOTH";
            $scope.cardctrl.showNFC = true;
            $scope.cardctrl.showQR = true;
         } else if ($scope.tokenmodel.nfc == true) {
            $scope.token.type = "NFC";
            $scope.cardctrl.showNFC = true;
            $scope.cardctrl.showQR = false;
         } else if ($scope.tokenmodel.qr == true) {
            $scope.token.type = "QR";
            $scope.cardctrl.showNFC = false;
            $scope.cardctrl.showQR = true;

        } else {
            $scope.token.type = "NONE";
            $scope.cardctrl.showNFC = false;
            $scope.cardctrl.showQR = false;
        }
    };

    $scope.exprirealter = function () {
        $scope.token.expire = moment().add($scope.tokenmodel.expire, 'hour').format('x');
    };


    function DialogController($scope, $mdDialog) {
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.qrdata = hash;
    }
    $scope.new = function()
    {
        $scope.cardctrl = {};
        $scope.cardctrl.data = "PayGram QR"
        $scope.cardctrl.visible = false;
        $scope.tokenmodel={};
        $scope.tokenmodel.nfc = null;
        $scope.tokenmodel.qr = null;
        $scope.tokenmodel.expire = 1;
        $scope.showopt = false;
        $scope.token = {};
        $scope.token.type = "";
        $scope.token.Amount = null;
        $scope.cardctrl.showQR = false;
        $scope.cardctrl.showNFC = false;
        $scope.token.expire = null;
        $scope.token.scheme = null;
        $scope.cardctrl.progress = false;
    };
    $scope.generate = function ()
    {
        if (validator()) {
            $scope.cardctrl.progress = true;
            $scope.token.Amount = parseInt($scope.token.Amount);
            var URL = "/api/" + $scope.token.scheme + "/payment_generator";
             $http.post(URL, JSON.stringify($scope.token))
                .then(function (res) {
                     $mdToast.show(
                         $mdToast.simple()
                             .content('TOKEN GENERATED SUCCESSFULLY')
                             .hideDelay(4000)
                     );
                     $scope.showopt = true;
                    if($scope.token.type =="BOTH" || $scope.token.type =="QR") {
                        $scope.cardctrl.QRvisible = true;

                     }
                     $scope.cardctrl.visible = true;

                     $scope.cardctrl.WriteNFC = function (ev) {
                          $mdDialog.show(
                             $mdDialog.alert()
                                 .title('Opening NFC writer')
                                 .content('Please open your Merchant APP and Stay online.your NFC writter will be launched soon')
                                 .targetEvent(ev)
                                 .ok('Got it!')
                         );
                          var token = {Token: res.data.TokenHASH};
                         Socket.emit(Evts.EVT_WRITE_NFC,token);


                     };
                     $scope.cardctrl.QRdialogue = function (ev) {
                         $mdDialog.show({
                             controller: DialogController,
                             template: '<md-dialog aria-label="">\n\n    <div class="panel panel-card p"  style="width:1000px" ng-init="model.size=200">\n        <div class="row" >\n            <div class="col-sm-6">\n                <p>\n                    <span class="label">Customize</span>\n                </p>\n                <div class="md-form-group">\n                    <input colorpicker class="md-input" value="#000" ng-model="background" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>Background</label>\n                </div>\n                <div class="md-form-group">\n                    <input class="md-input" colorpicker value="#fff"   ng-model="foreground" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>ForeGround</label>\n                </div>\n                <p>\n                <div class="md-form-group">\n                    <input class="md-input"  max="450"  min="150" type="number" ng-model="model.size" required="" tabindex="0" aria-required="true" aria-invalid="true" autocomplete="off" style="">\n                    </i><label>Size</label>\n                </div>\n                </p>\n\n\n            </div>\n\n            <div class="col-sm-6 mbr" >\n                <qrcode  class="qrshadow" version="8" size="{{model.size}}" download bg="{{background}}"  fg="{{foreground}}"\n                         error-correction-level="HIGH" data="{{qrdata}}"></qrcode>\n                <p class="text-muted mbr">Click the QR code To Download</p>\n            </div>\n\n        </div>\n    </div>\n\n\n    <div class="md-actions" layout="row">\n        <span flex></span>\n        <md-button ng-click="hide()" class="md-primary">\n            Close\n        </md-button>\n    </div>\n</md-dialog>\n',
                             targetEvent: ev
                         })

                     };
                     $scope.cardctrl.data = res.data.TokenHASH;
                    hash = res.data.TokenHASH;
                    setTimeout(function () {
                        $scope.cardctrl.progress = false;
                    }, 700);


                },
                function (response) {
                    $scope.cardctrl.progress = false;
                    $mdToast.show(
                        $mdToast.simple()
                            .content('ERROR Please Try Again')
                            .hideDelay(4000)
                    );
                });
        }

    };
    function validator() {
        if (parseInt($scope.token.Amount).toString() == "NaN") {
            $mdToast.show(
                $mdToast.simple()
                    .content('Please provide valid Amount')
                    .hideDelay(3000)
            );
            return false;
        } else if ($scope.token.Amount == null || $scope.token.Amount == "") {
            if (parseInt($scope.token.amount).toString() == "NaN") {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Please provide valid Amount')
                        .hideDelay(3000)
                );
                return false;
            }
        }
        else if ($scope.token.type == null || $scope.token.type == "") {
            $mdToast.show(
                $mdToast.simple()
                    .content('Error In  Type Field')
                    .hideDelay(3000)
            );
            return false;

        } else if ($scope.token.scheme == null || $scope.token.scheme == "") {
            $mdToast.show(
                $mdToast.simple()
                    .content('Error In Scheme Field')
                    .hideDelay(3000)
            );
            return false;

        } else if (parseInt($scope.token.Amount) < 10) {
            $mdToast.show(
                $mdToast.simple()
                    .content('Amount should be above Rs 10 ')
                    .hideDelay(4000)
            );
            return false;

        } else if ($scope.token.type == "NONE") {
            $mdToast.show(
                $mdToast.simple()
                    .content('Please Select a Token Type ')
                    .hideDelay(3500)
            );
            return false;
        } else {
            return true;

        }
    }


});
