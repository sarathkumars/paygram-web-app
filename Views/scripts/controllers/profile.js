/**
 * Created by Sarath Kumar on 29-12-2015.
 */

app.controller("profileCtrl", function (Auth, $scope,$http) {
    $scope.User = Auth.user;
    console.log($scope.User)
    $scope.EditUser = angular.copy($scope.User);
    if($scope.User.profilepic == null)
    {
        $scope.User.profilepic = "default.png";

    }
    if(Auth.Ismerchant){
        $scope.URL_UPLOAD = "/api/merchant/upload/";
        $scope.URL_USER_UPDATE = "/api/merchant/";
    } else{
        $scope.URL_UPLOAD = "/api/customer/upload/";
        $scope.URL_USER_UPDATE = "/api/merchant/";

    }
    $scope.SelectedImag='';
    $scope.CroppedImage='';
    $scope.cropType="circle";
    $scope.updateProfilePic = function(){
          $http.post($scope.URL_UPLOAD+$scope.User.id, JSON.stringify({bs64:$scope.CroppedImage}))
            .then(function(res)
            {
                   $scope.User =res.data;
                console.log($scope.User)
                    console.log("USER UPDATED");
            },
            function(response){
                console.log(response);

            } )
    };
    var handleFileSelect=function(evt)
    {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.SelectedImag=evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    $('#modal').on('show', function () {
        $(this).find('.modal-body').css({
            width:'auto', //probably not needed
            height:'auto', //probably not needed
            'max-height':'100%'
        });
    });
    $scope.saveUser = function(data){
        console.log($scope.URL_USER_UPDATE)
        
        $http.put($scope.URL_USER_UPDATE, JSON.stringify($scope.EditUser))
            .then(function(res)
            {
                $scope.User =res.data;
                console.log($scope.User)
                console.log("USER UPDATED");
            },
            function(response){
                console.log(response);
            } )
    }


});