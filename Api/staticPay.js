/**
 * Created by Sarath Kumar on 09-12-2015.
 */

var _ = require("underscore");
var moment = require("moment");
var crypto = require("crypto");
//PAYMENT PROCESS IS NOT COMPLETED

module.exports = function (app, db) {
    var middleware = require("./../middleware.js")(db, app);
    //GENERATE PAYMENT TOKEN FOR NFC AND QR CODE
    app.post('/api/static/payment_generator', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'Amount', 'type');
        if (typeof body.Amount === "number") {
            if (body.hasOwnProperty('type') && body.type === "NFC" || body.type == "QR"|| body.type == "BOTH") {
                var StaticPay = {
                    amount: body.Amount,
                    type: body.type,
                    Tokentype: "Static",
                    Expire: moment().add(120, "m").format('x')
                };
                if (body.hasOwnProperty('time') && typeof body.time === "number" && body.time >= 1 && body.time <= 120) {
                    StaticPay.Expire = moment().add(body.time, "m").format('x')
                }
                var randomString = crypto.randomBytes(32).toString('hex');
                db.Users.findById(req.current_user.get('id')).then(function (user) {
                StaticPay.UserId= user.get('id');
                StaticPay.TokenHASH = randomString;
                db.paymentTokens.create(StaticPay).then(function (staticPay) {
                         staticPay.TokenHASH = randomString;
                          res.json(staticPay);

                    });
                }, function (error) {
                    res.status(401).json(error)
                });

                return;


            }
            res.sendStatus(400);
        }
        res.sendStatus(400);

    });
     //PROCESS PAYMENT TOKEN FOR NFC AND QR CODE  PAYMENT
    //here We check wheather Token is valid
    app.post('/api/static/payment_processor', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'token');
        if (typeof body.token === "string" && body.token !== null) {

            db.paymentTokens.findOne(
                {
                    where: {TokenHASH: crypto.createHash('sha256').update(body.token).digest('base64'),Tokentype:"Static"}

                }).then(function (token) {
                    if (token == null) {
                        return res.sendStatus(400);
                    }
                    if (token.ispaid == true) {
                        return res.status(405).json({state: "INVALID PAYMENT..ALREADY PAID"})

                    }
                    var currentUnixStamp = moment().format('x');
                    if (token.Expire < currentUnixStamp) {
                        console.log("current", currentUnixStamp);
                        console.log("EXPIRE", token.Expire);
                        return res.status(405).json({state: "TOKEN EXPIRED"})
                    }
                    if (token.get('UserId') == req.current_user.get('id')) {
                        res.status(405).json({state: "YOU CAN'T PAY YOURSELF"})
                    } else {
                        if (req.current_user.Balance < token.amount) {

                            return res.status(405).json({state: "INSUFFICIENT BALANCE "})

                        } else {
                            db.Users.findById(token.get('UserId')).then(function (payee)
                            {
                                var processor = require("./../Modules/PaymentProcessor.js");
                                processor(db, req.current_user, payee, token, "Static").then(function (PaymentSucceed)
                                {
                                    console.log("i m here");

                                          res.json(PaymentSucceed);

                                }, function (error) {
                                    res.json(error);

                                });

                            }, function ()
                            {
                                console.log("NOT FOUND PAYEE");
                            });

                        }
                    }

                })

        }

    });
    app.post('/api/static/validate', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'token');
        if (typeof body.token === "string" && body.token !== null) {
            db.paymentTokens.findOne(
                {
                    where: {TokenHASH: crypto.createHash('sha256').update(body.token).digest('base64'),Tokentype:"Static"}
                }).then(function (token) {
                    if (token == null || token.ispaid == true) {
                        return res.sendStatus(400);
                    }
                    if (token.get('UserId') == req.current_user.get('id')) {
                        res.status(405).json({state: "YOU CAN't PAY YOURSELF"})
                    } else {
                        var currentUnixStamp = moment().format('x');
                        if (token.Expire < currentUnixStamp) {
                            console.log("current", currentUnixStamp);
                            console.log("EXPIRE", token.Expire);
                            res.status(405).json({state: "TOKEN EXPIRED"})
                        } else {
                            if (req.current_user.Balance < token.amount) {
                                return res.status(405).json({state: "INSUFFICIENT BALANCE"});
                            } else {

                                return res.status(200).json({state: "VALID"});
                            }
                        }

                    }

                })

        }

    });
    app.post('/api/static/getpayment', middleware.RequireAuth, function (req, res) {
        var body = _.pick(req.body, 'token');
        if (typeof body.token === "string" && body.token !== null) {
            db.paymentTokens.findOne(
                {
                    where: {TokenHASH: crypto.createHash('sha256').update(body.token).digest('base64'),Tokentype:"Static"}
                }).then(function (token) {
                    if (token == null) {
                        return res.sendStatus(400);
                    }
                    if (token.get('UserId') == req.current_user.get('id')) {
                        db.payments.findAll(
                            {
                                where: {
                                    PaymentTokenId: token.id
                                }
                            }
                        ).then(function (success) {
                                //RESPONSE
                                res.json(success);

                            }, function (error) {
                                console.log(error)
                            })

                    } else {

                        return res.status(401).json({state: "TOKEN OWNER ERROR"});

                    }

                })

        }

    });

};