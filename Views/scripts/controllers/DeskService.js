/**
 * Created by Sarath Kumar on 07-02-2016.
 */

app.factory("Clients", function () {

});
app.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});
app.controller("DeskMain", function (Auth, $scope, $http, $state, Evts, $mdToast, $mdDialog, $mdMedia, Socket,$rootScope) {
    var Stores = null;
    $scope.Status = "danger";
    $scope.CurrentStoreId=null;
    $scope.ConnectedTokens = [];
    $scope.ConnectedUsers = [];
    $scope.tableTokens = [];
    $scope.Products = [];
    $scope.orders =[];
    $rootScope.Activities =[];
    $rootScope.list =  {
        accepted:[],
        declined:[],
    };

    $scope.isConnected = function (id) {
        $scope.ConnectedTokens.forEach(function (data) {
            if (data == id) {
                return true
            }
        });
        return false;
    };
    $http.get("/api/stores").then(function (res) {
        if (res.data.length == 0) {
            $scope.stores = null;
            var confirm = $mdDialog.confirm()
                .title("you don't have any Stores!")
                .content('Please create at least one store to continue')
                .ariaLabel('ok')
                .ok('Please do it!')
                .cancel('Nop');
            $mdDialog.show(confirm).then(function () {
                $state.go('store.manage');
            }, function () {
                $state.go('app.dashboard');
            });

        } else {
            $scope.stores = res.data;
            Stores = res.data;
            $scope.ShowSelector();
        }
    }, function () {
    });
    $scope.ShowSelector = function () {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function ($scope) {
                $scope.stores = Stores;
                $scope.SelectStore = function (id) {
                    $mdDialog.hide(id);
                };
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            template: '<md-dialog aria-label="Select Your Store To configure" ng-cloak>\n    <form>\n        <md-toolbar>\n            <div class="md-toolbar-tools">\n                <h2>Select Your Store To configure\n                </h2>\n            </div>\n        </md-toolbar>\n        <md-dialog-content class="bg-light lt">\n            <div class="md-dialog-content ">\n                <h2>\n                    <div class="row">\n                        <div class="col-xs-6 col-md-4" ng-repeat="store in stores">\n                            <div class="card">\n                                <div class="item">\n                                    <div class="box bg-dark dk" style="padding-top: 15%;">\n                                        <i class="mdi-action-shopping-cart fa-4x" style="margin: 30%"></i>\n                                    </div>\n                                </div>\n                                <div class="container">\n                                    <h3>{{store.StoreName}}</h3>\n                                    <small>{{store.StoreTypes}}</small>\n                                </div>\n                                <div class="text-center">\n                                    <button ng-click="SelectStore(store.id)" md-ink-ripple="" class="md-btn md-raised m-b btn-fw bg-white">Launch\n                                        <div class="md-ripple-container"></div>\n                                    </button>\n                                </div>\n\n                            </div>\n                        </div>\n\n \n                    </div>\n            </div>\n            </h2>\n\n\n            </div>\n        </md-dialog-content>\n\n    </form>\n</md-dialog>',
            fullscreen: useFullScreen
        }).then(function (id) {
            $scope.InitDesk(id);
            $scope.CurrentStoreId=id;
        }, function () {

        });
    };

    $scope.InitDesk = function (id) {
        $scope.tableTokens = null;
        $scope.stores.forEach(function (e, index, arr) {
            if (id == e.id) {
                $scope.store = e;
            }
        });
        $http.get("/api/token/desktoken/" + $scope.store.id).then(function (res) {
            if (res.data.length == 0) {
                $scope.tableTokens = null;
            } else {
                console.log(res.data);
                $scope.tableTokens = res.data;
                $scope.showtime = true;
            }
        });
        if (Socket.CONNECTED)
        {
            Socket.emit(Evts.LAUNCH_DESK, {storeID: id});
            Socket.registerEvt(Evts.EVT_JOIN_REPLY);
            $scope.load_products(id);
        }

    };
    $scope.RestartService =function(){
        $scope.Status = "danger";
        $rootScope.Activities.push({time:new Date().getTime(),activity:"Service Restart Initiated"});
        $scope.ConnectedTokens = [];
        $scope.ConnectedUsers = [];
        $scope.Products = [];
        $scope.orders =[];
        $rootScope.list =  {
            accepted:[],
            declined:[],
        };
        Socket.emit(Evts.LAUNCH_DESK, {storeID: $scope.CurrentStoreId});

    };
    $scope.load_products = function (id) {
        $http.get("/api/products/" +  id)
            .then(function (res) {
                if (res.data) {
                    $scope.Products = res.data;
                }
            });
    };

    $scope.$on(Evts.EVT_STATES, function (evnts, data) {
        if (data.state == "DESK") {
            if (data.payload == true) {
                $scope.Status = 'success'
                $rootScope.Activities.push({time:new Date().getTime(),activity:"Service Started"});

            }
        }

    });

    $scope.$on(Evts.EVT_JOIN_REPLY, function (evnts, data) {
        console.log("REPALY");
        console.log(data);
        jQuery("#notification_newclient")[0].play();
        var token = $.grep($scope.tableTokens, function (table) {
            return table.id == data.tokenID;
        });
        $scope.ConnectedTokens.push(data.token);
        $http.get("/api/customer/" + data.uid).then(function (res) {
            console.log(res)
            if (res.data) {

                res.data.table = token[0];
                $scope.ConnectedUsers.push(res.data);
                console.log($scope.ConnectedUsers)
            }
        });
        var elm = $("#" + data.tokenID);
        if (elm.hasClass('animated')) {
            elm.removeClass("animated");
            elm.removeClass("flip");
        }
        setTimeout(function () {
            elm.addClass("animated");
            elm.removeClass("light-blue-500");
            elm.addClass("green-500");
            elm.addClass("flip");
            $scope.$apply();
        }, 100);
        $scope.$apply();
        SpawnNotifiation(null,"New Client","New user Connected on table-"+token[0].DeskName);
        $rootScope.Activities.push({time:new Date().getTime(),activity:"Client Connected on TABLE-"+token[0].DeskName});

    });

    $scope.$on(Evts.EVT_LEAVE, function (evnts, data) {
        console.log("REPALY");
        console.log(data);
        var index = $scope.ConnectedTokens.indexOf(data.token);
        if (index > -1) {
            $scope.ConnectedTokens.splice(index, 1);
        }
        var UserInfo = $.grep($scope.ConnectedUsers, function (userInfo) {
            console.log("itraion uid", data.uid)
            console.log("itraion", userInfo)

            return userInfo.id == data.uid;
        });
        var userindex = $scope.ConnectedUsers.indexOf(UserInfo[0]);
        console.log("founded index ", userindex);
        if (userindex > -1) {
            $scope.ConnectedUsers.splice(userindex, 1);
            console.log($scope.ConnectedUsers)
        }
        var elm = $("#" + data.tokenID);
        if (elm.hasClass('animated')) {
            elm.removeClass("animated");
            elm.removeClass("flip");
        }
        setTimeout(function () {
            elm.removeClass("green-500");
            elm.addClass("animated");
            elm.addClass("light-blue-500");
            elm.addClass("flip");
            $scope.$apply();
        }, 100);
        $rootScope.Activities.push({time:new Date().getTime(),activity:"Client Disconnected on TABLE-"+data.token.DeskName});


    });

    $scope.$on(Evts.EVT_DISCONNECT, function () {
        $scope.Status = 'danger'

    });

    $scope.$on(Evts.EVT_RECONNECT, function () {
        $scope.Status = 'success';
        Socket.emit(Evts.RE_INIT_STORE, {storeID: id});
        Socket.registerEvt(Evts.RE_INIT_STORE);

    });

    $scope.$on(Evts.RE_INIT_STORE, function () {
        $scope.Status = 'success';
        Socket.emit(Evts.RE_INIT_STORE, {storeID: id});
        Socket.registerEvt(Evts.RE_INIT_STORE);

    });

    $scope.$on(Evts.EVT_ORDER, function (event, data) {
        jQuery("#notification_fitterbug")[0].play();

        $scope.orders.push(data);
        var product = $.grep($scope.Products, function (product) {
            return product.id == data.productID;
        });
        var User = $.grep($scope.ConnectedUsers, function (userInfo) {
            return userInfo.id == data.uid;
        });
        var Token = $.grep($scope.tableTokens, function (tokens) {
            return tokens.id == data.tokenID;
        });
        order = {
            product: product[0],
            user: User[0],
            msg: data.msg,
            qty: data.qty,
            token:Token[0],
            orderTime:new Date().getTime()
        };
        SpawnNotifiation(null,"New Order Received ","Table-"+Token[0].DeskName+" By user-"+User[0].Name);
        $scope.ShowOrder(order,true,"New Order Received | Table-"+Token[0].DeskName+" | user-"+User[0].Name)
        $rootScope.Activities.push({time:new Date().getTime(),activity:"New Order Received on TABLE-"+Token[0].DeskName});

    });

    $scope.ShowOrder = function (Orderdata,check,heading) {
        var CurrentOrder = Orderdata;

        console.log(CurrentOrder);
        $mdDialog.show({
            controller: function ($scope,Socket,Evts) {
                $scope.orderinfo = CurrentOrder;
                $scope.check = check;
                $scope.product = CurrentOrder.product;
                $scope.User = CurrentOrder.user;
                $scope.Token = CurrentOrder.token;

                $scope.OrderText = heading;

                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };

                $scope.AcceptOrder = function(){
                    Socket.emit(Evts.EVT_ORDER_RPLY,{tokenid:$scope.Token.id,productID:$scope.product.id,accept:true})
                    $rootScope.list.accepted.push(CurrentOrder);
                    if($scope.check==true)
                    {
                        $rootScope.list.declined.pop(CurrentOrder);

                    }
                    $mdDialog.hide();
                };
                $scope.DeclineOrder=function(){
                    Socket.emit(Evts.EVT_ORDER_RPLY,{tokenid:$scope.Token.id,productID:$scope.product.id,accept:false})
                    $rootScope.list.declined.push(CurrentOrder);
                    $mdDialog.hide( );
                } ;
            },
            clickOutsideToClose: true,
            parent: angular.element(document.body),
            template: "<md-dialog aria-label=\"{{OrderText}}\" ng-cloak>\n         <md-toolbar>\n            <div class=\"md-toolbar-tools\">\n                <h2>{{OrderText}}\n                </h2>\n            </div>\n        </md-toolbar>\n        <md-dialog-content class=\"bg-light lt\" style=\"\n    width: 800px;>\n            \n\">\n\n            <div class=\"row row-xs ng-scope\">\n                <div class=\"col-sm-6\">\n                    <div class=\"panel panel-card\">\n                        <div class=\"item\">\n                            <img ng-src=\"/product/{{product.productpic}}\" class=\"w-full r-t\" alt=\"Washed Out\">\n\n                            <div class=\"bottom text-white p\" style=\"    height: 100;\n\">\n                                {{product.Name}}\n                            </div>\n                        </div>\n                        <button md-ink-ripple=\"\" class=\"md-btn md-fab md-raised bg-white m-r md-fab-offset pull-right\">\n                            {{product.Price}}\n                        </button>\n                        <div class=\"p\">\n                            <h3> {{product.Name}}</h3>\n\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-6\">\n\n                    <div class=\"panel panel-card\">\n                        <div class=\"panel-heading b-b b-light\">Order summary</div>\n                        <div class=\"list-group no-border no-radius\">\n                            <div class=\"list-group-item\">\n                                <span class=\"pull-right\">{{product.Name}}</span>\n                                <i class=\"mdi-maps-local-mall m-r-sm text-info\"></i>\n                                Product\n                            </div>\n                            <div class=\"list-group-item\">\n                                <span class=\"pull-right\">{{orderinfo.qty}}</span>\n                                <i class=\"mdi-content-content-copy m-r-sm text-info\"></i>\n                                Quantity\n                            </div>\n                            <div class=\"list-group-item\">\n                                <span class=\"pull-right\">{{Token.DeskName}}</span>\n                                <i class=\"mdi-editor-border-all m-r-sm text-info\"></i>\n                                Table\n                            </div>\n                            <div class=\"list-group-item\">\n                                <span class=\"pull-right\">{{orderinfo.orderTime | date:\'medium\'}}</span>\n                                <i class=\"mdi-device-access-time m-r-sm text-info\"></i>\n                                Ordered At\n                            </div>\n                            <div class=\"list-group-item\" style=\'overflow: auto \'>\n\n                                <span class=\"pull-left\">  <i class=\"fa fa-rupee m-r-sm text-info\"></i>  <small>\n                                    Total price\n                                </small></span>\n                                <span class=\"pull-right\"> <i class=\"fa fa-rupee text-success\"></i>  <small>  {{product.Price * orderinfo.qty }}</small></span>\n\n\n                            </div>\n                            <div class=\"list-group-item\" style=\'overflow: auto \'>\n\n                                <span class=\"pull-left\">  <i class=\"mdi-communication-message m-r-sm text-info\"></i>  <small>\n                                    Message\n                                </small></span>\n                                <span class=\"pull-right\">   </i><small>{{orderinfo.msg}}</small></span>\n                            </div>                      \n\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"row no-gutter\">\n                <div class=\'col-md-4 col-md-offset-8\'>\n                    <button class=\"btn btn-rounded btn-stroke btn-success\"  ng-if=\"check\" ng-click=\"AcceptOrder()\">Accept Order</button>\n                    <span flex></span>\n                    <button class=\"btn btn-rounded btn-stroke btn-danger\" ng-if=\"check\" ng-click=\"DeclineOrder()\">Decline Order</button>\n                </div>\n            </div>\n        </md-dialog-content>\n \n</md-dialog>",
        }).
            then(function (id) {
            }, function () {
            });
    };


})
;
function SpawnNotifiation(icon,title,body) {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    else if (Notification.permission === "granted") {

        var options = {
            body: body,
            icon: icon
        };
        var n = new Notification(title,options);
    }

    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                var options = {
                    body: body,
                    icon: icon
                };
                var n = new Notification(title,options);
            }
        });
    }

    // At last, if the user has denied notifications, and you
    // want to be respectful there is no need to bother them any more.
}

app.controller("DeskRecents", function () {



    /*END CTRl*/
});

