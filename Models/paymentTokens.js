/**
 * Created by Sarath Kumar on 22-12-2015.
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('PaymentTokens', {
        amount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true,
            min: 1,
            max: 50000
        },
        TokenHASH: {
            type: DataTypes.STRING,
            allowNull: false,
            min: 15,
            unique: true
        },
        UseCounter: {
            type: DataTypes.INTEGER
        },

        StoreId: {
            type: DataTypes.INTEGER,
            allowNull: true

        },
        Expire: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        Desktoken: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: true
        },
        DeskName: {
            type: DataTypes.STRING,
            defaultValue: "DESK",
            allowNull: true
        },
        ApiId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: -1
        },
        callback: {
            type: DataTypes.STRING,
            defaultValue: "null",
            allowNull: true
        },

        ispaid: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        type: {
            type: DataTypes.STRING,
            validate: {
                isIn: [['NFC', 'QR', 'BOTH']]
            }
        }, Tokentype: {
            type: DataTypes.STRING,
            validate: {
                isIn: [['Dynamic', 'Static', 'Donate']]
            }
        }
    });
};

