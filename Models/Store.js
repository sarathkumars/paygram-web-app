/**
 * Created by Sarath Kumar on 10-01-2016.
 */


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Store', {
        StoreName: {
            type: DataTypes.STRING,
            allowNull: false

        },
        StoreDescription: {
            type:DataTypes.STRING(1000),
            allowNull: true
        },
        OpenTime: {
            type:DataTypes.DATE,
            allowNull: true
        },
        CloseTime: {
            type:DataTypes.DATE,
            allowNull: true
        },
        StoreTypes: {
            type:DataTypes.STRING,
            allowNull: false
        },
        Address: {
            type:DataTypes.STRING,
            allowNull: false
        },
        StorePic: {
            type:DataTypes.STRING,
            allowNull: true
        }
    });

};