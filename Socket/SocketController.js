/**
 * Created by Sarath Kumar on 13-01-2016.
 */
var JWT = require("jsonwebtoken");
var crypto = require("crypto-js");
var _ = require("underscore");
var Evnts = require("./../Socket/SocketEvnts.js");
var cookie = require('cookie');
var moment = require("moment");

module.exports = function (http, db) {
    var ConnectionMngr = require("./../Utils/ConnectionManager.js");
    var io = require('socket.io')(http);
    io.on(Evnts.EVT_CONNECT, function (socket) {
        console.log("CONNECTED WITH ID" + socket.id);
        TokenAuth(socket, db, function (token) {
            if (token != false && token != undefined && token != null) {
                if (token.user == "WEB") {
                    db.Users.findById(token.id).then(function (User) {
                        if (User) {
                            console.log("ADDING CONNECTION");
                            ConnectionMngr.AddConnection(User, socket, ConnectionMngr.WEBAPP);
                            setTimeout(function () {
                                if(ConnectionMngr.OnlineCheck(User.id).MOB){
                                    socket.emit(Evnts.MOB_STATE, true);
                                }
                             }, 1000);

                        }

                    }, function (error) {

                    })
                } else if (token.user == "MOB") {
                    db.Users.findById(token.id).then(function (User) {
                        if (User) {
                            console.log("ADDING CONNECTION");
                            ConnectionMngr.AddConnection(User, socket, ConnectionMngr.MOBILE);

                            if (ConnectionMngr.OnlineCheck(User.id).WEB == true) {
                                var Socket_Web = ConnectionMngr.GetSocketByUserid(User.id, ConnectionMngr.WEBAPP);

                                Socket_Web.emit(Evnts.MOB_STATE, true);
                            }
                        }

                    }, function (error) {

                    })
                }

            }
        });
        socket.on(Evnts.EVT_DISCONNECT, function () {
            console.log("DISCONNECTED ID" + socket.id);
            try {
                if (ConnectionMngr.GetUserBySocket(socket, ConnectionMngr.MOBILE)) {
                    var User = ConnectionMngr.GetUserBySocket(socket, ConnectionMngr.MOBILE);

                    var Socket_Web = ConnectionMngr.GetSocketByUserid(User.id, ConnectionMngr.WEBAPP);
                    Socket_Web.emit(Evnts.MOB_STATE, false);

                }
            }catch(err){

            }
            ConnectionMngr.RemoveConnection(socket)


        });
        require("../Socket/paymentEvents")(socket, db, io);
        require("../Socket/tapNorderService")(socket, db, io);
        require("../Socket/GeneralEvents")(socket, db, io);

        socket.on(Evnts.EVT_WRITE_NFC, function (data)
        {
            var user = ConnectionMngr.GetUserBySocket(socket,ConnectionMngr.WEBAPP);
            console.log("GOT EVT", user.id)
            var state = ConnectionMngr.OnlineCheck(user.id);
            console.log(state)
            if (state.MOB) {
                var Socket_Mobile = ConnectionMngr.GetSocketByUserid(user.id, ConnectionMngr.MOBILE);
                Socket_Mobile.emit(Evnts.EVT_WRITE_NFC, data);
                console.log(data)
                console.log("MOB SENT")

            }

        });



    });


};

function TokenAuth(socket, db, callback) {
    if (socket.request.headers != undefined && socket.request.headers.cookie != undefined) {
        if (cookie.parse(socket.request.headers.cookie).AcNtknCK != undefined && cookie.parse(socket.request.headers.cookie).AcNtknCK != null) {
            {
                var token = cookie.parse(socket.request.headers.cookie).AcNtknCK;
                db.token.findOne({
                    where: {
                        tokenHash: crypto.MD5(token).toString()
                    }
                }).then(function (tokenInstance) {

                    if (tokenInstance == null) {
                        socket.emit(Evnts.EVT_LOGOUT);
                        socket.disconnect();
                    } else {
                        var decode = Decodetoken(token);
                        decode.user = "WEB";
                        callback(decode);
                    }

                }, function (error) {
                    socket.emit(Evnts.EVT_LOGOUT);
                    socket.disconnect();
                    return false;
                });
            }
        }
    }
    else if (socket.request.headers.token != null || socket.request.headers.token != undefined) {
        var token = socket.request.headers.token;
        db.token.findOne({
            where: {
                tokenHash: crypto.MD5(token).toString()
            }
        }).then(function (tokenInstance) {
            if (tokenInstance == null) {
                socket.emit(Evnts.EVT_LOGOUT);
                socket.disconnect();
                return false;

            } else {
                var decode = Decodetoken(token);
                decode.user = "MOB"
                callback(decode);
            }

        }, function (error) {
            socket.emit(Evnts.EVT_LOGOUT);
            socket.disconnect();
            return false;

        });
    } else {
        socket.emit(Evnts.EVT_LOGOUT);
        socket.disconnect();

    }


}
function Decodetoken(token) {
    var decodedJWT = JWT.verify(token, 'qwerty098');
    var bytes = crypto.AES.decrypt(decodedJWT.token, 'abc123!@#!');
    return JSON.parse(bytes.toString(crypto.enc.Utf8));
}
