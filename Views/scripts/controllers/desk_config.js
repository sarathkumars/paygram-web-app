/**
 * Created by Sarath Kumar on 06-02-2016.
 */
app.controller("deskConfig", function (Auth, $scope, $http, $state,Evts, $mdToast, $mdDialog, $mdMedia, Socket) {
    var Stores;
    $scope.showtime=false;
    $scope.showToken = false;
    $scope.span=2;
    $http.get("/api/stores").then(function (res) {
        if (res.data.length == 0) {
            $scope.stores = null;
            var confirm = $mdDialog.confirm()
                .title("you don't have any Stores!")
                .content('Please create at least one store to continue')
                .ariaLabel('ok')
                .ok('Please do it!')
                .cancel('Nop');
            $mdDialog.show(confirm).then(function () {
                $state.go('store.manage');

            }, function () {
                $state.go('app.dashboard');

            });

        } else {
            $scope.stores = res.data;
            Stores = res.data;
            $scope.ShowSelector();
        }
    }, function () {

    });
     $scope.ShowSelector = function () {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function ($scope) {
                $scope.stores = Stores;
                $scope.SelectStore = function (id) {
                    $mdDialog.hide(id);
                };
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            template: '<md-dialog aria-label="Select Your Store To configure" ng-cloak>\n    <form>\n        <md-toolbar>\n            <div class="md-toolbar-tools">\n                <h2>Select Your Store To configure\n                </h2>\n            </div>\n        </md-toolbar>\n        <md-dialog-content class="bg-light lt">\n            <div class="md-dialog-content ">\n                <h2>\n                    <div class="row">\n                        <div class="col-xs-6 col-md-4" ng-repeat="store in stores">\n                            <div class="card">\n                                <div class="item">\n                                    <div class="box bg-dark dk" style="padding-top: 15%;">\n                                        <i class="mdi-action-shopping-cart fa-4x" style="margin: 30%"></i>\n                                    </div>\n                                </div>\n                                <div class="container">\n                                    <h3>{{store.StoreName}}</h3>\n                                    <small>{{store.StoreTypes}}</small>\n                                </div>\n                                <div class="text-center">\n                                    <button ng-click="SelectStore(store.id)" md-ink-ripple="" class="md-btn md-raised m-b btn-fw bg-white">Config\n                                        <div class="md-ripple-container"></div>\n                                    </button>\n                                </div>\n\n                            </div>\n                        </div>\n\n \n                    </div>\n            </div>\n            </h2>\n\n\n            </div>\n        </md-dialog-content>\n\n    </form>\n</md-dialog>',
            fullscreen: useFullScreen
        }).then(function (id) {
            $scope.initConfigScreen(id);
         }, function () {

        });
    };


      $scope.initConfigScreen = function(id){
         $scope.desktokens = null;
         $scope.stores.forEach(function(e,index,arr){
              if(id== e.id){
                  $scope.store=e;
              }
         });
         $http.get("/api/token/desktoken/"+ $scope.store.id).then(function (res) {
             if (res.data.length == 0) {
                 $scope.desktokens = null;
             } else {
                 console.log( res.data);

                 $scope.desktokens =  res.data;
                 $scope.showtime = true;
             }
         }, function () {
             $scope.desktokens = null;
         });

         $scope.CreateToken = function(){
             $mdDialog.show({
                 controller: function ($scope) {
                     $scope.cancel = function () {
                         $mdDialog.cancel();
                     };
                     $scope.create = function (Name) {
                       if(Name.toString().length<=1){

                       }  else{
                           $mdDialog.hide(Name);
                       }

                     };

                 },
                 template: '\n<md-dialog aria-label="e" ng-cloak>\n    <md-toolbar>\n        <div class="md-toolbar-tools">\n            <h2>Create Desk Token</h2>\n            <span flex></span>\n            <md-button class="md-icon-button" ng-click="ctrl.cancel()">\n                <i class="fa fa-close" ng-click="cancel()"></i>\n            </md-button>\n        </div>\n    </md-toolbar>\n    <md-dialog-content>\n        <div class="md-dialog-content">\n            <form ng-submit="$event.preventDefault()" >\n                <p> Enter your Desk name <code>eg :Desk 2,left-1-3...</code></p>\n                <md-input-container>\n                    <label>Title</label>\n                    <input ng-model="name">\n                </md-input-container>\n                <md-button  aria-label="Finished" ng-click="create(name)">Create</md-button>\n\n            </form>\n        </div>\n    </md-dialog-content>\n  \n</md-dialog>\n\n\n\n',
              }).then(function (name) {
                 $http.post("/api/token/desktoken/"+ $scope.store.id, JSON.stringify({DeskName:name}))
                     .then(function(){
                         $scope.initConfigScreen();
                     },function(){

                     });
             }, function () {

             });

         };
         $scope.SelectedToken=null;
          $scope.showTokenInfo = function(token,elm){
              $scope.showToken=true;
              $scope.SelectedToken=token;
              $scope.qrdata =   token.TokenHASH;
                 var tokenHASH =$scope.SelectedToken.TokenHASH;
              $scope.WriteNFC = function (ev) {
                  $mdDialog.show(
                      $mdDialog.alert()
                          .title('Opening NFC writer')
                          .content('Please open your Merchant APP and Stay online.your NFC writter will be launched soon')
                          .targetEvent(ev)
                          .ok('Got it!')
                  );
                  var payload = {Token: $scope.SelectedToken.TokenHASH};
                  Socket.emit(Evts.EVT_WRITE_NFC,payload);

              };
              $scope.QRdialogue = function (ev) {
                  $mdDialog.show({
                      controller: function($scope){
                              $scope.token = tokenHASH;
                              $scope.hide =function(){
                                  $mdDialog.cancel();
                              }
                      },
                      template: "<md-dialog aria-label=\"\">\n\n    <div class=\"panel panel-card p\"  style=\"width:1000px\" ng-init=\"model.size=200\">\n        <div class=\"row\" >\n            <div class=\"col-sm-6\">\n                <p>\n                    <span class=\"label\">Customize</span>\n                </p>\n                <div class=\"md-form-group\">\n                    <input colorpicker class=\"md-input\" value=\"#000\" ng-model=\"background\" required=\"\" tabindex=\"0\" aria-required=\"true\" aria-invalid=\"true\" autocomplete=\"off\" style=\"\">\n                    </i><label>Background</label>\n                </div>\n                <div class=\"md-form-group\">\n                    <input class=\"md-input\" colorpicker value=\"#fff\"   ng-model=\"foreground\" required=\"\" tabindex=\"0\" aria-required=\"true\" aria-invalid=\"true\" autocomplete=\"off\" style=\"\">\n                    </i><label>ForeGround</label>\n                </div>\n                <p>\n                <div class=\"md-form-group\">\n                    <input class=\"md-input\"  max=\"450\"  min=\"150\" type=\"number\" ng-model=\"model.size\" required=\"\" tabindex=\"0\" aria-required=\"true\" aria-invalid=\"true\" autocomplete=\"off\" style=\"\">\n                    </i><label>Size</label>\n                </div>\n                </p>\n\n\n            </div>\n\n            <div class=\"col-sm-6 mbr\" >\n                <qrcode  class=\"qrshadow\" version=\"8\" size=\"{{model.size}}\" download bg=\"{{background}}\"  fg=\"{{foreground}}\"\n                         error-correction-level=\"HIGH\" data=\"{{token}}\"></qrcode>\n                <p class=\"text-muted mbr\">Click the QR code To Download</p>\n            </div>\n\n        </div>\n    </div>\n\n\n    <div class=\"md-actions\" layout=\"row\">\n        <span flex></span>\n        <md-button ng-click=\"hide()\" class=\"md-primary\">\n            Close\n        </md-button>\n    </div>\n</md-dialog>\n",
                      targetEvent: ev
                  })

              };
               $scope.deletetoken =function(ev){
                        var confirm = $mdDialog.confirm()
                           .title('Alert')
                           .content('Do you Want to continue')
                           .ariaLabel('Lucky day')
                           .ok('Please do it!')
                           .cancel('nop')
                           .targetEvent(ev);

                       $mdDialog.show(confirm).then(function() {
                           $http.delete("/api/token/"+ $scope.SelectedToken.id)
                               .then(function(){
                                   $scope.initConfigScreen();
                               },function(){

                               });
                       }, function() {

                       });


               }

          }
     }


});