/**
 * Created by Sarath Kumar on 17-04-2016.
 */

app.controller("Apicontroller", function (Auth, $state, $scope, $http, Socket, Evts, $rootScope) {
    $scope.api = {}
    $scope.api.Key = "*********************************************";
    $scope.api.createdAt = null;
    $scope.api.updatedAt = null;
    $scope.api.id = null;
    $scope.foundKey = false;
    $scope.progress = false;


    $scope.getKey = function(){
        $scope.progress = true;
        $http.get("/api/getKey").then(function (res) {
            $scope.progress=false;
            if (res.data) {
                if (res.data.status) {
                    $scope.api = res.data.api;
                    $scope.foundKey = true;

                }
            }
        });

    };
    $scope.getKey();

    $scope.GenerateKey = function () {
        $scope.progress=true;
        $http.post("/api/createkey").then(function (res) {
            console.log(res);
            if (res.data) {
                $scope.api = res.data;
                $scope.foundKey = true;
                $scope.progress=false;

            }
        });
    };
    $scope.DeleteKey = function () {
        $scope.progress=true;
         $http.post("/api/deletkey").then(function (res) {

            console.log(res);
            if (res.data) {
                if (res.data.status == "Success") {
                    $scope.api = {}
                    $scope.api.Key = "*********************************************";
                    $scope.api.createdAt = null;
                    $scope.api.updatedAt = null;
                    $scope.api.id = null;
                    $scope.foundKey = false;
                    $scope.progress=false;

                }
            }
        });
    }

});