/**
 * Created by Sarath Kumar on 05-12-2015.
 */
var crypto = require("crypto-js");
var JWT = require("jsonwebtoken");
var USER_CUSTOMER="customer";
var USER_ADMIN="admin";
var USER_MERCHANT="merchant";
module.exports = function(db,app){
  return {
        RequireMerchant:function(req,res,next){
            Authenticator(req,db).then(function(User){
                if(req.UserType === "Merchant"){
                    next();
                }else{
                    res.sendStatus("401" )

                }
            },function(errorCode){
                res.sendStatus("401" )
            })

        },
      RequireAuth:function(req,res,next){
            Authenticator(req,db).then(function(User){
                    next();

            },function(errorCode){
                res.sendStatus(401)
            })

        },
      RequireCustomer:function(req,res,next){
          Authenticator(req,db).then(function(User){
              if(req.UserType === "Customer"){
                  next();
              } else{
                  res.sendStatus("401" )

              }
          },function(errorCode){
              res.sendStatus("401" )
          })

      },
      /**
       * @return {boolean}
       */
      TokenDecode :function(token){
          var decoded=null;
          try{
               decoded = Decodetoken(token);
          }catch(Error){
              console.log(error);
              return false;
          }
          return decoded;
      }
    }
};

function Decodetoken(token){
    var decodedJWT = JWT.verify(token, 'qwerty098');
    var bytes = crypto.AES.decrypt(decodedJWT.token, 'abc123!@#!');
    return JSON.parse(bytes.toString(crypto.enc.Utf8));
}
function Authenticator(req,db){
    return new Promise(function(resolve,reject){
        var token = req.get('AcNtkn') || null;
        if(token===null){
            token = req.cookies.AcNtknCK;
         }
        if(!token || token==null || token ==undefined){
            reject(400);
        }
        db.token.findOne({
            where: {
                tokenHash: crypto.MD5(token).toString()
            }
        }).then(function (tokenInstance)
        {
            if (!tokenInstance) {
                throw new Error();
            }
            try{
                var decoded = Decodetoken(token);
            }catch(Error){
                console.log(error);
                 reject(400);
            }
            switch (decoded.payload.type){
                case USER_CUSTOMER:
                    db.Users.findOne({
                        where:{
                            id: decoded.id
                        }
                    }).then(function(customer){
                        req.token =tokenInstance;
                        req.current_user =customer;
                        req.UserType = "Customer";
                        resolve(customer);
                    } );


                    break;
                case  USER_ADMIN:

                    break;

                case  USER_MERCHANT:
                    db.Users.findOne({
                        where:{
                            id: decoded.id
                        }
                    }).then(function(merchant){
                        req.token =tokenInstance;
                        req.current_user =merchant;
                        req.UserType = "Merchant";
                        resolve(merchant);
                    } );
                    break;

            }

        }).catch(function () {
            reject(401);
        });
    })

}