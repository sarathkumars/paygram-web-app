'use strict';

/**
 * @ngdoc function
 * @name app.config:uiRouter
 * @description
 * # Config
 * Config for the router
 */
angular.module('app')
    .config(
    ['$stateProvider', '$urlRouterProvider', 'MODULE_CONFIG', 'MODULE_CONFIG',
        function ($stateProvider, $urlRouterProvider, MODULE_CONFIG) {
            $urlRouterProvider
                .otherwise('app/dashboard');
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/app',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html',
                            resolve: load(['scripts/controllers/chart.js', 'scripts/controllers/vectormap.js'])

                        },
                        'content': {
                            templateUrl: 'views/content.html'
                        }
                    }
                })
                .state('app.dashboard', {
                    url: '/dashboard',
                    requireAuth: true,
                    templateUrl: 'views/pages/dashboard.html',
                    data: {title: 'Dashboard', folded: false},

                })
                .state('app.wall', {
                    url: '/wall',
                    templateUrl: 'views/pages/dashboard.wall.html',
                    data: {title: 'Wall', folded: true}
                })
                .state('desk',
                {
                    url: '/tapNorder',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html'
                        },
                        'content': {
                            templateUrl: 'views/content.html'
                        }
                    }
                })
                .state('desk.config', {
                    url: '/config',
                    requireAuth: true,
                     data: {title: ' Tap N Order Configuration', folded: true},
                    templateUrl: 'views/pages/Desk_config.html',
                    resolve: load(['scripts/controllers/desk_config.js', 'ngImgCrop', 'libs/qrcode.min.js', 'scripts/directives/angular-qrcode.js']),
                    controller: 'deskConfig'

                })
                .state('desk.service', {
                    url: '/service',
                    requireAuth: true,
                    data: {title: 'PayGram Tap N Order', folded: true},
                    templateUrl: 'views/pages/DeskService_main.html',
                    resolve: load(['scripts/controllers/DeskService.js', 'ngImgCrop', 'libs/qrcode.min.js', 'scripts/directives/angular-qrcode.js']),
                    controller: 'DeskMain'

                })
                .state('paytokens',
                {
                    url: '/paytokens',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html'
                        },
                        'content': {
                            templateUrl: 'views/content.html'
                        }
                    }
                })
                .state('paytokens.generate', {
                    url: '/generate',
                    requireAuth: true,
                    data: {folded: false},
                    templateUrl: 'views/pages/generate.html',
                    resolve: load(['scripts/controllers/TokenGenerator.js', 'libs/qrcode.min.js', 'scripts/directives/tokencard.js', 'scripts/directives/angular-qrcode.js']),
                    controller: 'GeneratorCtrl'
                })
                .state('paytokens.tokens', {
                    url: '/tokens',
                    requireAuth: true,
                    data: {folded: false},
                    templateUrl: 'views/pages/tokens.html',
                    resolve: load(['scripts/controllers/tokenlist.js', 'libs/qrcode.min.js', 'scripts/directives/tokencard.js', 'scripts/directives/angular-qrcode.js']),
                    controller: 'tokenctrl'
                })


                .state('paytokens.tokens.list', {
                    url: '/list/{fold}',
                    templateUrl: 'views/partials/tokenlist.html'
                })


                .state('store',
                {
                    url: '/store',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html'
                        },
                        'content': {
                            templateUrl: 'views/content.html'
                        }
                    }
                })
                .state('store.manage', {
                    url: '/manage',
                    requireAuth: true,
                    data: {folded: false},
                    templateUrl: 'views/pages/store_manage.html',
                    resolve: load(['scripts/controllers/store_manage.js', 'ngImgCrop', 'libs/qrcode.min.js', 'scripts/directives/angular-qrcode.js']),
                    controller: 'storeManage'
                })
                .state('store.products', {
                    url: '/products',
                    requireAuth: true,
                    templateUrl: 'views/pages/productmanage.html',
                    controller: 'ProductManage',
                    resolve: load(['scripts/controllers/ProductManager.js', 'angular-img-cropper'])

                })
                .state('user',
                {
                     url: '/user',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html'
                        },
                        'content': {
                            templateUrl: 'views/content.html'
                        }
                    }
                })
                .state('service',
                {
                     url: '/service',
                    views: {
                        '': {
                            templateUrl: 'views/layout.html'
                        },
                        'aside': {
                            templateUrl: 'rest/aside.html'
                        },
                        'content': {
                            templateUrl: 'views/contentApi.html'
                        }
                    }
                })
                .state('user.profile', {
                    url: '/profile',
                    requireAuth: true,
                    templateUrl: 'views/pages/profile.html',
                    controller: 'profileCtrl',

                })

                .state('service.api', {
                    url: '/api',
                     templateUrl: 'views/pages/api.html',
                    data: {title: 'API',folded: true},
                    resolve: load(['libs/prism.js','libs/prism.css','scripts/controllers/apiController.js'])   ,
                    controller: 'Apicontroller'
                })
                .state('user.blank', {
                    url: '/blank',
                    templateUrl: 'views/pages/blank.html',
                    data: {title: 'Blank'}
                })
                .state('user.document', {
                    url: '/document',
                    templateUrl: 'views/pages/document.html',
                    data: {title: 'Document'}
                })
                .state('404', {
                    url: '/404',
                    templateUrl: 'views/pages/404.html'

                })
                .state('505', {
                    url: '/505',
                    templateUrl: 'views/pages/505.html'
                })
                .state('access', {
                    url: '/access',
                    template: '<div class="bg-dark bg-big"><div ui-view class="fade-in-right-big smooth"></div></div>'
                })
                .state('access.signin', {
                    url: '/signin',
                    requireAuth: false,
                    templateUrl: 'views/pages/signin.html',
                    controller: 'signIn',
                    resolve: load(['scripts/controllers/signin.js'])
                })
                .state('access.signup', {
                    url: '/signup',
                    templateUrl: 'views/pages/signup.html',
                    resolve: load(['ngImgCrop', 'angularFileUpload', 'angularFileUpload', 'scripts/controllers/sign_up.js', 'scripts/controllers/imgcrop.js'])
                })
                .state('access.forgot-password', {
                    url: '/forgot-password',
                    templateUrl: 'views/pages/forgot-password.html'
                })
                .state('access.lockme', {
                    url: '/lockme',
                    templateUrl: 'views/pages/lockme.html'
                });

            function load(srcs, callback) {
                return {
                    deps: ['$ocLazyLoad', '$q',
                        function ($ocLazyLoad, $q) {
                            var deferred = $q.defer();
                            var promise = false;
                            srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                            if (!promise) {
                                promise = deferred.promise;
                            }
                            angular.forEach(srcs, function (src) {
                                promise = promise.then(function () {
                                    angular.forEach(MODULE_CONFIG, function (module) {
                                        if (module.name == src) {
                                            if (!module.module) {
                                                name = module.files;
                                            } else {
                                                name = module.name;
                                            }
                                        } else {
                                            name = src;
                                        }
                                    });
                                    return $ocLazyLoad.load(name);
                                });
                            });
                            deferred.resolve();
                            return callback ? promise.then(function () {
                                return callback();
                            }) : promise;
                        }]

                }
            }
        }
    ]
);
