/**
 * Created by Sarath Kumar on 23-12-2015.
 */
'use strict';

/* Controllers */
app.controller('signIn', function($scope,$http,$mdToast,$state,Auth) {
    $scope.signin =function()
    {
     var URL = "";
     if($scope.user.Email !=undefined && $scope.user.password !==undefined){
         URL = "/api/merchant/login";
         console.log(URL);
          $http.post(URL, JSON.stringify($scope.user))
             .then(function(res)
             {
                 Auth.user = res.data;
                   $mdToast.show(
                     $mdToast.simple()
                         .content('Login Success Welcome! '+res.data.Name)
                         .hideDelay(3000));
                 $state.go('app.dashboard');

             },
             function(response){
                     $mdToast.show(
                     $mdToast.simple()
                         .content('Error! Check Your Email and Password')
                         .hideDelay(3000));

            } )

     }
     }

});
