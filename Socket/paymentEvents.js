/**
 * Created by Sarath Kumar on 22-01-2016.
 */
var Evnts = require("./../Socket/SocketEvnts.js");
var _ = require("underscore");
var ConnectionMngr = require("./../Utils/ConnectionManager.js");
var moment = require("moment");
var desk = require('../Utils/DeskConnectionsManger');

module.exports = function (socket, db, io) {

    socket.on(Evnts.EVT_CODE_VALIDATE, function (data) {
        console.log("GOT TOKEN: "+ data.code);
        db.paymentTokens.findOne({
            where: {
                TokenHASH: data.code
            }
        }).then(function (token) {
            if (token)
            {
                token = JSON.parse(JSON.stringify(token));
                token.isvalid = true;
                token.state = 200;
                 if(token.StoreId==null){
                     db.Users.findById(token.UserId).then(function (user) {
                          token.merchantName =   user.Name;
                          token.Profile =   user.profilepic;
                          socket.emit(Evnts.EVT_VALIDATE_REPLY, token)
                     }, function (error) {
                         var reply  = {
                             isvalid: false,
                             state:404
                         };
                         socket.emit(Evnts.EVT_VALIDATE_REPLY, reply )
                     });
                 } else if(token.UserId==null)
                 {
                     db.Store.findById(token.StoreId).then(function (store) {
                         token.merchantName =   store.StoreName;
                         token.Profile =   store.StorePic;
                         if(desk.CheckOnline(store.id)){
                             token.service =true;
                         } else{
                             token.service =false;

                         }
                         socket.emit(Evnts.EVT_VALIDATE_REPLY, token)
                     }, function (error) {
                         var reply  = {
                             isvalid: false,
                             state:404
                         };
                         socket.emit(Evnts.EVT_VALIDATE_REPLY, reply )
                     });
                 }

            } else
            {
                var reply  = {
                    isvalid: false,
                    state:404
                };
                socket.emit(Evnts.EVT_VALIDATE_REPLY, reply )
            }
        }, function (data) {
            var reply  = {
                isvalid: false,
                state:505
            };
            socket.emit(Evnts.EVT_VALIDATE_REPLY, reply)
        })
    });


    socket.on(Evnts.EVT_PROCESS_PAYMENT, function (data) {
        var CurrentUser = ConnectionMngr.GetUserBySocket(socket, ConnectionMngr.MOBILE);
        CurrentUser.reload().then(function ()
        {
            db.paymentTokens.findOne(
                {
                    where: {TokenHASH: data.token, Tokentype: data.Tokentype}

                }).then(function (token) {
                    if (token == null) {
                        var object = {
                            status: "error",
                            message: "NOT FOUND"
                        };
                        socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object);
                        return;

                    }
                    if (token.ispaid == true) {
                        var object = {
                            status: "error",
                            message: "INVALID PAYMENT..ALREADY PAID"
                        };
                        socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object);
                        return;
                    }
                    var currentUnixStamp = moment().format('x');
                    if (token.Expire < currentUnixStamp) {
                        console.log("current", currentUnixStamp);
                        console.log("EXPIRE", token.Expire);
                        var object = {
                            status: "error",
                            message: "EXPIRE"
                        };
                        socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object)
                    }

                    if (token.get('UserId') == CurrentUser.id) {

                        var object = {
                            status: "error",
                            message: "YOU CAN't"
                        };
                        socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object)
                    } else {
                        if (CurrentUser.Balance < token.amount) {
                            console.log("CURR BAL", CurrentUser.Balance);
                            console.log("TOk BAL", token.amount);

                            var object = {
                                status: "error",
                                message: "INSUFFICIENT BALANCE"
                            };
                            socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object)

                        } else {
                            db.Users.findById(token.get('UserId')).then(function (payee)
                            {
                                var processor = require("./../Modules/PaymentProcessor.js");
                                processor(db, CurrentUser, payee, token, token.Tokentype).then(function (PaymentSucceed) {

                                    var count = token.UseCounter + 1;
                                    token.update({UseCounter: count, ispaid: true}).then(function () {
                                        var object = {
                                            status: "SUCCESS",
                                            message: "SUCCESS"
                                        };
                                        socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object);
                                        SentNotify(socket, payee, CurrentUser);

                                    });

                                }, function (error) {
                                    var object = {
                                        status: "error",
                                        message: "ERORRRRR"
                                    };
                                    socket.emit(Evnts.EVT_PROCESS_PAYMENT_REPLY, object)
                                });

                            }, function () {
                                console.log("NOT FOUND PAYEE");
                            });

                        }
                    }

                })
        });

    });

    function SentNotify(payersocket, payee, Payer) {
        var state = ConnectionMngr.OnlineCheck(payee.id);
        if (state.WEB) {
            payeesocket = ConnectionMngr.GetSocketByUserid(payee.id, ConnectionMngr.WEBAPP);
            try {
                var payload = {payee: Payer.Name, photo: Payer.profilepic};
                payeesocket.emit(Evnts.EVT_NOITFY,payload);
            } catch (error) {
                console.log(error)

            }
        }


    }

};
