/**
 * Created by Sarath Kumar on 10-04-2016.
 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('PaymentCards',
        {
            card_number: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            expire_month: {
                type: DataTypes.STRING,
                allowNull: false
            },
            expire_year: {
                type: DataTypes.STRING,
                allowNull: false
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            card_type: {
                type: DataTypes.STRING,
                allowNull: false
            }
        }
    );
};
