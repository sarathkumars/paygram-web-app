/**
 * Created by Sarath Kumar on 03-02-2016.
 */
app.controller("ProductManage", function (Auth, $scope, $http, $mdToast) {
    $scope.stores = null;
    $scope.selectedIndex = 0;
    $scope.model = {};
    $scope.ShowResult = false;
    $scope.selectstore = null;
    $scope.tabCtrl = true;
    $scope.cropper = {};
    $scope.cropper.sourceImage = null;
    $scope.cropper.croppedImage = null;
    $scope.bounds = {};
    $scope.bounds.left = 0;
    $scope.bounds.right = 0;
    $scope.bounds.top = 0;
    $scope.bounds.bottom = 0;
    $scope.products = {};
    $http.get("/api/stores").then(function (res) {
        if (res.data.length == 0) {
            $scope.stores = null;
        } else {
            $scope.stores = res.data;
            $scope.intitproducts();
        }
    }, function () {

    });
    $scope.intitproducts = function(){
        $http.get("/api/stores").then(function (res) {
            $scope.ShowResult=false;
            if (res.data.length == 0) {
                $scope.stores = null;
            } else {
                $scope.stores = res.data;
                $scope.stores.forEach(function (element, index, array) {
                        $http.get("/api/products/"+element.id).then(function (res) {
                            $scope.products[element.id] = res.data;
                            if(index==($scope.stores.length-1))  {
                                $scope.ShowResult=true;
                            }
                        }, function () {
                        })
                    }
                )
            }
        }, function () {
        });
    };

    $scope.change = function (val) {
        $scope.selectstore = parseInt(document.getElementById("Storeid").value);
        console.log($scope.selectstore);
    };

    $scope.submit = function () {
        var URL = "api/product/create/" + $scope.selectstore;
        $http.post(URL, JSON.stringify($scope.model)).then(function (res) {
            $scope.tabCtrl = false;
            $scope.selectedIndex = 1;
            $scope.product = res.data;
            $mdToast.show(
                $mdToast.simple()
                    .content('Product Added Successfully,now upload product Image')
                    .hideDelay(3000));

        }, function () {
            $mdToast.show(
                $mdToast.simple()
                    .content('Error')
                    .hideDelay(3000));
        });
    };
    $scope.deleteProduct = function(id){
        $http.delete("/api/product/"+id).then(function (res) {
            $mdToast.show(
                $mdToast.simple()
                    .content('product deleted...')
                    .hideDelay(3000));
             $scope.intitproducts();
        }, function () {
        });
    };

    $scope.upload = function () {
        var URL = "/api/product/pic/" + $scope.product.id;
        $http.post(URL, JSON.stringify({bs64: $scope.cropper.croppedImage})).then(function (res) {
            $mdToast.show(
                $mdToast.simple()
                    .content('Success..')
                    .hideDelay(3000));
            $scope.intitproducts();


        }, function () {
            $mdToast.show(
                $mdToast.simple()
                    .content('ERROR..')
                    .hideDelay(3000));
        });
    };


});
