/**
 * Created by Sarath Kumar on 13-01-2016.
 */
app.factory("Socket", function ($rootScope,Evts,$http,$state) {
    var Socket = io.connect();
    Socket.CONNECTED = false;
    Socket.on(Evts.EVT_CONNECT, function () {
        $rootScope.$broadcast(Evts.EVT_CONNECT, null);
        console.log("CONNECTED");
        Socket.CONNECTED = true;
    });

    Socket.on(Evts.EVT_DISCONNECT, function () {
        $rootScope.$broadcast(Evts.EVT_DISCONNECT, null);
         Socket.CONNECTED = false;
    });

    Socket.on(Evts.EVT_RECONNECTING, function () {
        $rootScope.$broadcast(Evts.EVT_RECONNECTING, null);

    });
    Socket.on(Evts.EVT_RECONNECT_ER, function () {
        $rootScope.$broadcast(Evts.EVT_RECONNECT_ER, null);

    });
    Socket.on(Evts.EVT_RECONNECT, function () {
        $rootScope.$broadcast(Evts.EVT_RECONNECT, null);
        Socket.CONNECTED = true;
        console.log()

    });
    Socket.on(Evts.EVT_NOITFY, function (data) {
        $rootScope.$broadcast(Evts.EVT_NOITFY, data);

    });
    Socket.on(Evts.MOB_STATE, function (data) {
        console.log(data);
        console.log("GOT MOB STAT")
        $rootScope.$broadcast(Evts.MOB_STATE, data);

    });
    Socket.on(Evts.EVT_STATES, function (data) {
        $rootScope.$broadcast(Evts.EVT_STATES, data);
        console.log("CONNECTED");
    });
     Socket.on(Evts.EVT_JOIN_REPLY, function (data) {
        $rootScope.$broadcast(Evts.EVT_STATES, data);
        console.log("USEER CONNECTED");
    });

    Socket.on(Evts.EVT_LOGOUT, function () {
        $rootScope.$broadcast(Evts.EVT_LOGOUT, null);
        $http({
            method: 'POST',
            url: '/api/merchant/logOut'
        }).then(function(){
            $state.go('access.signin', {}, {reload: true});
            console.log($state)
        },function(){
            $state.go('access.signin', {}, {reload: true});

        });
        console.log("RECONNECT error");
    });
    Socket.on(Evts.EVT_ORDER, function (data) {
        console.log("ORDER CALL") ;
        $rootScope.$broadcast(Evts.EVT_ORDER, data);

    });
 Socket.on(Evts.EVT_LEAVE, function (data) {
        console.log(data);
        $rootScope.$broadcast(Evts.EVT_LEAVE, data);

    });

    Socket.registerEvt=function(Evt){
        Socket.on(Evt,function(data){
            $rootScope.$broadcast(Evt, data);

        })
    };
    Socket.reconnecter = function(){
        if(Socket && Socket.socket) {
            Socket.socket.reconnect()
        }
    };
    return Socket;

});